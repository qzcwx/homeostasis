function generate_minizinc_static(ADJ,N,K,fname)
%% This function generates a .mzn file
% It is just for on-cyclic behavior tuning
%% inputs: 
% ADJ:  Adjacency Matrix 4*N , where N is the number of variables in the
% network, 1st row is the Target, 2nd Row Source, 3rd Weight, 4th Parity(0,1,-1) 
% N :  vector of Max transcription level!
% fname = name of the file to be created
% >> Note: any unknown parameter is denoted by -1 in the matrix
%% output : 
% Generates fname.mzn in current directory of matlab.
%% Example 1:
% HPA Axis:
% ADJ =  [1 2 3 2 4 4;
%         3 1 2 4 3 4;
%         1 1 1 1 2 2;
%         0 1 1 0 1 1];
% N = [1 1 2 2];
% generate_minizinc(ADJ,N);
%% Example 2:
% HPA Axis:
% ADJ =  [1 2 3 2 4 4;
%         3 1 2 4 3 4;
%         1 1 1 1 -1 -1; % Edge weights not Known
%         0 1 -1 0 1 1]; % One parity not known
% N = [1 1 2 2];
% generate_minizinc(ADJ,N);
%% Example 3:
% HPA Axis:
% ADJ =  [1 2 3 2 4 4;
%         3 1 2 4 3 4;
%         1 1 1 1 -1 -1; % Edge weights not Known
%         0 1 -1 0 1 1]; % One parity not known
% N = [1 1 2 2];
% K = [0 -1 0 0; % Some K values known
%      0 1 1 1;
%      0 2 0 0;
%      0 -1 -1 -1;];
% generate_minizinc(ADJ,N,K);
%%

if nargin < 4
   fname = 'test.mzn'; 
   if nargin < 3
     K = [];
      if nargin < 2
       error('Provide N and ADJ!') 
      end
   end
end

if ~strcmp(fname(end-2:end),'mzn')
   fname = [fname,'.mzn'];
end

% create a file in the folder
fid = fopen(fname,'w');
fprintf(fid,'%s\n\n',['/* This is an automatically',...
                     ' generated script for minizinc.',...
                     ' For further information consult',...
                     ' generate_minizinc.m function.',...
                     ' Coded by Hooman Sedghamiz, March 2017 */']); 
%% Start writing the file
fprintf(fid,'%s\n',['int: N = ',mat2str(size(ADJ,2)),';']);                % total number of Edges

fprintf(fid,'%s','array[int] of int: ANN = [');                            % Max Transcription level
for i = 1 : length(N)
    if i ~= length(N)
    fprintf(fid,'%s',[mat2str(N(i)),',']);
    else
    fprintf(fid,'%s\n',[mat2str(N(i)),'];']);    
    end
end

fprintf(fid,'%s\n',['int: M_L = ',mat2str(max(N)),';']);                 % Max transc all can take

fprintf(fid,'%s','set of int: M_W = {');                                   % Max possible value for edges
for i = 1 : length(N)
    if i ~= length(N)
    fprintf(fid,'%s',[mat2str(N(i)),',']);
    else
    fprintf(fid,'%s\n',[mat2str(N(i)),'};']);    
    end
end

%fprintf(fid,'%s','array[int] of set of int: E_I = [');    
indeg = cell(1,length(N));                                                 % Compute indegree for each variable
TL = 0;
for i = 1 : length(N)
    ind = find(ADJ(1,:) == i);
    if length(ind) > TL
       TL = length(ind);                                                   % Max indegree in net 
    end
    S = ADJ(2,ind);
    indeg{1,i}= [ind;S];
%     for j = 1: length(ind)
%        if j == 1 && (length(ind) > 1)
%           fprintf(fid,'%s',['{',mat2str(ind(j)),',']);  
%        elseif j == length(ind) && (length(ind) > 1)
%           if i < length(N)
%              fprintf(fid,'%s',[mat2str(ind(j)),'},']); 
%           else
%              fprintf(fid,'%s',[mat2str(ind(j)),'}']);  
%           end
%        elseif length(ind)==1 
%           if  i < length(N) 
%              fprintf(fid,'%s',['{',mat2str(ind(j)),'},']); 
%           else  
%              fprintf(fid,'%s',['{',mat2str(ind(j)),'}']); 
%           end
%        else
%           fprintf(fid,'%s',[mat2str(ind(j)),',']); 
%        end
%     end
end

%fprintf(fid,'%s\n','];');   

fprintf(fid,'%s\n',['int: Max_IE = ',mat2str(TL),';']);                    % Max indegree in net 


fprintf(fid,'%s','set of int: RangeK = {');                                % Set of int that K values might assume
for i = 0 : max(N)
    if i ~= max(N)
    fprintf(fid,'%s',[mat2str(i),',']);
    else
    fprintf(fid,'%s\n',[mat2str(i),'};']);    
    end
end


fprintf(fid,'%s','array[int,int] of int: K_t = [');                        % K values matrix
ff = 0;
NoC = 0; % dont post constraints on K values if we have them all

if isempty(K)
   K = zeros(length(N),2^TL);                                              % if K empty initialize 
   ff = 1;
end

for i = 1 : size(K,1)
    fprintf(fid,'%s','|');
    if ff 
      K(i,1:2^size(indeg{1,i},2)) = -1;
      %K(i,1)=0; % sets all basals to zero
    end
    for j = 1: size(K,2)
      if j ~= size(K,2)
         fprintf(fid,'%s',[mat2str(K(i,j)),',']);
      else
          if i < size(K,1)
            fprintf(fid,'%s\n',mat2str(K(i,j)));  
          else
            fprintf(fid,'%s',mat2str(K(i,j)));    
          end
      end
    end
end

fprintf(fid,'%s\n\n','|];'); 

fprintf(fid,'%s','array[int,int] of int: ADJ = [');                        % ADJ matrix

for i = 1 : size(ADJ,1)
    fprintf(fid,'%s','|');
    for j = 1: size(ADJ,2)
      if j ~= size(ADJ,2)
         fprintf(fid,'%s',[mat2str(ADJ(i,j)),',']);
      else
          if i < size(ADJ,1)
            fprintf(fid,'%s\n',mat2str(ADJ(i,j)));  
          else
            fprintf(fid,'%s',mat2str(ADJ(i,j)));    
          end
      end
    end
end
fprintf(fid,'%s\n\n','|];'); 

%%                                                                          Routine Constraints and Instantiations
fprintf(fid,'%s\n','int: NN = length(ANN);');
fprintf(fid,'%s\n','int: S_K = pow(2,Max_IE);');
fprintf(fid,'%s\n','array[1..N] of var bool: U;');
fprintf(fid,'%s\n','array[1..N] of var M_W: W;');
fprintf(fid,'%s\n','array[1..NN] of var 0..M_L: X;');
fprintf(fid,'%s\n\n','array[1..NN,1..S_K] of var RangeK: K;');
fprintf(fid,'%s\n','% Routine Constraints');
%fprintf(fid,'%s\n\n','constraint forall(i in 1..NN)(forall(j in 1..S_K where j > pow(2,length(E_I[i])))(K[i,j]=0));');
fprintf(fid,'%s\n\n','constraint forall(i in 1..NN,j in 1..S_K)(if K_t[i,j] != -1 then K[i,j]=K_t[i,j] else true endif );');
fprintf(fid,'%s\n\n','constraint forall(i in 1..NN,j in 1..S_K)(K[i,j]<=ANN[i]);');
fprintf(fid,'%s\n\n','constraint forall(i in 1..NN)(X[i]<=ANN[i]);');
fprintf(fid,'%s\n\n','constraint forall(i in 1..N)(if ADJ[4,i] != -1 then U[i] = ADJ[4,i] else true endif);');
fprintf(fid,'%s\n\n','constraint forall(i in 1..N)(if ADJ[3,i] != -1 then W[i] = ADJ[3,i] else true endif);');

%% Here we encode constraints if we have info about attractors
fprintf(fid,'\n\n');
fprintf(fid,'%s\n\n','% Constraints about attractors');

%% Here we encode constraints on K values
fprintf(fid,'\n\n');
fprintf(fid,'%s\n\n','% Constraints on K values');

K_ind = generate_K(N,ADJ); % generates a set of indice for K
 
if ~NoC
 % First K10 < K12 <...
 for i = 1: length(K_ind)
   for j = 1: size(K_ind{1,i},2)-1
       for k = j+1 : size(K_ind{1,i},2)
           
           % first constraint basal K values
           if j == 1 
               TF = [];
              % check for parity of all edges assoc with K
              for m = 1: size(K_ind{1,i},1)-1
                 if K_ind{1,i}(m,k) 
                    % edge sign , lazy to preallocate
                    TF = [TF ADJ(4,ind_adj(ADJ,K_ind{1,i}(m,k),i))];
                 end
              end
              if all(TF) || all(~TF)
                 fprintf(fid,'%s\n',...
                     ['constraint ','K[',mat2str(i),',',mat2str(j),...
                     ']',' <= ','K[',mat2str(i),',',mat2str(k),'];']);                                
              end
              
             % Now general case 
           else
              TF = [];
              L = [];
              % check for parity of all edges assoc with K
              for m = 1: size(K_ind{1,i},1)-1
                 if K_ind{1,i}(m,j) 
                    % edge sign  
                    TF = [TF ADJ(4,ind_adj(ADJ,K_ind{1,i}(m,j),i))];
                    L = [L K_ind{1,i}(m,j)];
                 end
              end
              % if the K11 has all same parity then check the rest
              if all(TF) || all(~TF) % if all activators 
                  TF1 = [];
                  L1 = [];
                 % check for parity of all edges assoc with K
                 for m = 1: size(K_ind{1,i},1)-1
                    if K_ind{1,i}(m,k) 
                        % edge sign  
                      TF1 = [TF1 ADJ(4,ind_adj(ADJ,K_ind{1,i}(m,k),i))];
                      L1 = [L1 K_ind{1,i}(m,k)];
                    end
                 end
                 % First check whether both have the same sign
                 if and(all(TF),all(TF1)) || and(all(~TF),all(~TF1))
                     % now check whether one subsumes the other
                     if length(TF) < length(TF1)
                        YN = false(1,length(TF));
                        for jj = 1: length(TF)
                            YN(jj) = any(L1 == L(jj));
                        end
                        
                        if all(YN)
                          fprintf(fid,'%s\n',...
                         ['constraint ','K[',mat2str(i),',',mat2str(j),...
                         ']',' <= ','K[',mat2str(i),',',mat2str(k),'];']);   
                        end
                     end
                 end
              end      
           end
           
       end
   end 
 end
end
fprintf(fid,'\n\n');
fprintf(fid,'%s\n\n','% Constraints based on System of Equations');
%% System of Equations 
F = cell(1,length(N));
for i = 1: length(N)
    for j = 1: size(K_ind{1,i},2)
        
  
           F{1,i} = [F{1,i},' K[',mat2str(i),...
                   ',',mat2str(j),']'];
        
        % Basal Value multiplies all
        if j ==1
           
            for k = 1: size(indeg{1,i},2)
                      F{1,i} = [F{1,i},'*(1-TH(X[',...
                      mat2str(indeg{1,i}(2,k)),...
                      ']',',','W[',mat2str(indeg{1,i}(1,k)),...
                      '], U[',...
                      mat2str(indeg{1,i}(1,k)),']))']; %mat2str(i)
                      if k == size(indeg{1,i},2)
                         F{1,i} = [F{1,i},'+']; 
                      end       
            end
          

        % Now general case    
        else
         
           indeg{1,i}(3,:)=0; 
           for jj = 1: size(K_ind{1,i},1)-1
               if K_ind{1,i}(jj,j)
                  A = indeg{1,i}(2,:)== K_ind{1,i}(jj,j);
                  if ~isempty(A)
                    indeg{1,i}(3,A)=1;
                  end
               end
           end
           
           for k = 1: size(indeg{1,i},2)         
                  if indeg{1,i}(3,k)
                        str = '*(';
                  else
                        str = '*(1 - ';
                  end                 
                      F{1,i} = [F{1,i},str,'TH(X[',...
                      mat2str(indeg{1,i}(2,k)),...
                      ']',',','W[',mat2str(indeg{1,i}(1,k)),...
                      '], U[',...
                      mat2str(indeg{1,i}(1,k)),']))'];   %mat2str(i)
                  if k == size(indeg{1,i},2)
                      if j ~= size(K_ind{1,i},2)
                         F{1,i} = [F{1,i},'+'];   
                      end
                  end  
           end 
        
        end    
    end
    if ~isempty(indeg{1,i}) % if that node has at least one indegree   
        F{1,i} =['(',F{1,i},')'];
        F1 = ['constraint X[',mat2str(i),']=',F{1,i},';'];
        fprintf(fid,'%s\n\n',F1);
    end
end

%% Solve Satisfy
fprintf(fid,'%s\n\n','solve satisfy;');

%% Internal Threshold Function
fprintf(fid,'%s\n','function var 0..1: TH(var int: X, var int: A, var bool: B) =');
fprintf(fid,'%s\n','let {var 0..1: F;');
fprintf(fid,'%s\n','constraint if (X>=A /\ B) \/ (X<A /\ not(B)) then');
fprintf(fid,'%s\n','   F = true      % Activator Above TH');
fprintf(fid,'%s\n','else');
fprintf(fid,'%s\n','   F = false     % Activator below TH');
fprintf(fid,'%s\n','endif} in F;');

fclose(fid);
end

function k = generate_K(N,Ad)
%% Generating Combinatorial K parameter Set
% I am not preallocating here cause its a combinatorial issue, If I do, I
% have to generate a matrix with the length of sum(N!/(N - r)!r!) which
% grows exponentially with the interconnectivity of the network! Now I use
% the ability of Matlab in generating Cell structure!

L = length(N);
k = cell(1,L);

 for i = 1: L  
   k{1,i}(1,1) = i;                     % Ki0 is the minimum transcription
   LI = (Ad(1,:)== i);                  % The nodes which act on Xi
   st_t = Ad(2,LI);                     % Index of Vertex acting on Xi
   LL = length(st_t);                   
   offset = 2;
   for j = 1: LL  
     ind = nchoosek(st_t,j);                           % Binomial pick up
     LJ = size(ind);
     k{1,i}(1:LJ(2),offset:offset + LJ(1) - 1) = ind'; 
     offset = offset + LJ(1);
   end 
    k{1,i}(end+1,:) = -1;
    k{1,i} = single(k{1,i});% convert to single for saving memory
 end  
end

function ind = ind_adj(ADJ,source,target)
%% returns index of an edge in ADJ
 for i = 1: size(ADJ,2)
    if ADJ(1,i) == target && ADJ(2,i) == source
       ind = i;
       break;
    end
 end

end