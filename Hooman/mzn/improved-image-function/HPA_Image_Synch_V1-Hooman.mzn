/* This is an automatically generated script for minizinc. For further information consult generate_minizinc_cycle.m function. Coded by Hooman Sedghamiz, March 2017 */

int: N = 8;
array[int] of int: ANN = [1,1,2,2];
int: M_L = 2;
set of int: M_W = {1,1,2,2};
int: Max_IE = 2;
set of int: RangeK = {0,1,2};
array[int,int] of int: K_t = [|-1,-1,-1,-1
|-1,-1,-1,-1
|-1,-1,-1,-1
|-1,-1,-1,-1|];

array[int,int] of int: ADJ = [|1,2,2,3,4,4,1,3  %(The last two edges are spurious)
|3,1,4,2,3,4,4,1,
|-1,-1,-1,-1,-1,-1,-1,-1
|0,1,0,1,1,1,-1,-1
|-1,-1,-1,-1,-1,-1,-1,-1|]; %5th -1 means not sure if the edge should be there or not, 1 means it should be there! (Here we are checking all the edges)

int: NN = length(ANN);
int: S_K = pow(2,Max_IE);
array[1..N] of var bool: U;
array[1..N] of var bool: EM;
array[1..N] of var M_W: W;
array[1..NN,1..S_K] of var RangeK: K;

% Routine Constraints
constraint forall(i in 1..NN,j in 1..S_K)(if K_t[i,j] != -1 then K[i,j]=K_t[i,j] else true endif );

constraint forall(i in 1..NN,j in 1..S_K)(K[i,j]<=ANN[i]);

constraint forall(i in 1..N)(if ADJ[4,i] != -1 then U[i] = ADJ[4,i] else true endif);

constraint forall(i in 1..N)(if ADJ[3,i] != -1 then W[i] = ADJ[3,i] else W[i] <= ANN[ADJ[2,i]] endif);

constraint forall(i in 1..N)(if ADJ[5,i] != -1 then EM[i] = ADJ[5,i] else true endif);



% Constraints about Cycle
array[1..L1,1..NN] of var 0..M_L: x1;
array[1..L1,1..NN] of var 0..M_L: X1;
int: L1=8;

array[int,int] of int: Tr1 = [|0,0,0,0
|1,1,0,0
|1,1,1,0
|0,1,2,0
|0,1,2,1
|0,0,2,1
|0,0,1,1
|0,0,0,0|];
constraint cycleconstraint(x1, X1, Tr1, L1);

array[1..L2,1..NN] of var 0..M_L: x2;
array[1..L2,1..NN] of var 0..M_L: X2;
int: L2=8;

array[int,int] of int: Tr2 = [|0,0,2,2
|0,0,1,2
|0,0,0,2
|1,0,0,2
|1,1,0,2
|1,1,1,2
|0,1,2,2
|0,0,2,2|];
constraint cycleconstraint(x2, X2, Tr2, L2);



% Constraints on K values

constraint K[1,1] <= K[1,2];
constraint K[2,1] <= K[2,2];
constraint K[2,1] <= K[2,3];
constraint K[3,1] <= K[3,2];
constraint K[4,1] <= K[4,2];
constraint K[4,1] <= K[4,3];
constraint K[4,1] <= K[4,4];
constraint K[4,2] <= K[4,4];
constraint K[4,3] <= K[4,4];

% Holds index of incoming edges 
array[1..NN, 1..Max_IE] of var 0..N: in_edges;

constraint forall(target in 1..NN)(
  let { int: LL = sum(in_deg in 1..N)(bool2int(ADJ[1,in_deg]==target));                          % number of Indegs for target 
        array[1..LL] of int: temp_index = ([in_deg|in_deg in 1..N where ADJ[1,in_deg]==target]); % Holds index of indegs to target
      } in forall(i in 1..LL)(
           in_edges[target,i] = temp_index[i]
      ) /\ if LL < Max_IE then
              forall(i in LL+1..Max_IE)(in_edges[target,i] = 0)
           else 
              true
           endif                                      
);

% basis for computing index to K
array[1..Max_IE] of int: base = [pow(2,i) | i in 0..Max_IE-1];



var int : A;
constraint A = sum(i in 1..N)(EM[i]*W[i]); % minimize sum of EM and W
solve minimize A;
output["EM = ",show(EM),"\n"];
output["W = ",show(W),"\n"];
output["A = ", show(A), "\n"];

%% Threshold function: 
% X: current state
% A: weight
% B: sign
function var bool: TH(var int: X, var int: A, var bool: B) = 
if (X>=A /\ B) \/ (X<A /\ not(B)) then
  true       % Activator Above TH
else
  false      % Activator below TH
endif;

%% delay function
% x[i]: current state
% X: image state
% x[i+1]: successor state
predicate delay(array[int,int] of var int: x, array[int,int] of var int: X, int: nrow) = forall(row in 1..nrow-1, i in 1..NN) (
(X[row,i] > x[row,i] /\ x[row + 1, i] = x[row,i] + 1) \/ 
(X[row,i] < x[row,i] /\ x[row + 1, i] = x[row,i] - 1) \/ 
(X[row,i] = x[row,i] /\ x[row + 1, i] = x[row,i])
);

predicate cycleconstraint(array[int,int] of var int: x, array[int,int] of var int: X, array[int,int] of int: cycle, int: nrow) = 
forall(i in 1..nrow, j in 1..NN) (if cycle[i,j]!=-1 then x[i,j]=cycle[i,j] else true endif) /\
forall(row in 1..nrow) (                                                        %for a cycle of size nrow,the number of transition is nrow
forall(target in 1..NN)(
  let { array[1..Max_IE] of var bool: order_mask;                               % Order Mask
        constraint forall(i in 1..Max_IE) (                                         
        (order_mask[i] = true                                                   % for the incoming edge to be active
        <->       
%         in_edges[target,i] != 0 /\ 
        TH(x[row, ADJ[2,in_edges[target,i]]],                                   % source node status, 2nd row in ADJ is the source node
          W[in_edges[target,i]],                                                % temp_index[i] returns the edge index
          U[in_edges[target,i]]) /\ EM[in_edges[target,i]]));
  } in K[target, sum([base[i] * bool2int(order_mask[i]) | i in 1..Max_IE where in_edges[target,i]!=0]) + 1]
       = X[row,target] /\ forall(target in 1..NN)(X[row,target]<=ANN[target])
)) /\ delay(x, X, nrow);                                                        % Time Treatment

