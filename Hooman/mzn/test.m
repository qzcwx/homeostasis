% HPA
matFile = '/home/chenwx/workspace/Homeostasis/Hooman/MainFunctions_V1/example.mat';
load(matFile);
ADJ = example.GeneralLogic.Ad;
% separate sign and weight absolute value
ADJ(4,:) = ADJ(3,:) > 0; 
ADJ(3,:) = abs(ADJ(3,:));
generate_minizinc_chen(ADJ, [], '/home/chenwx/workspace/Homeostasis/ConstraintProgramming/hpa.mzn', '/home/chenwx/workspace/Homeostasis/py/sync.cycle');

% %% HPAG
% matFile = '/home/chenwx/workspace/Homeostasis/Hooman/HPAG/HPAG.mat';
% load(matFile);
% ADJ = HPAG.interaction;
% ADJ(4,:) = ADJ(3,:) > 0;
% ADJ(3,:) = abs(ADJ(3,:));
% generate_minizinc_chen(ADJ, [], '/home/chenwx/workspace/Homeostasis/ConstraintProgramming/test.mzn', '/home/chenwx/workspace/Homeostasis/py/hpag-sync.cycle');

% %% D Cell
% matFile = '/home/chenwx/workspace/Homeostasis/Hooman/mzn/BIBE/Networks_results_BIBE_2017_pp/DentriticNet.mat';
% load(matFile);
% ADJ = Dendritic.interaction;
% ADJ(4,:) = ADJ(3,:) > 0; 
% ADJ(3,:) = abs(ADJ(3,:));
% generate_minizinc_chen(ADJ, [], '/home/chenwx/workspace/Homeostasis/ConstraintProgramming/test.mzn', '/home/chenwx/workspace/Homeostasis/py/sync.cycle');
