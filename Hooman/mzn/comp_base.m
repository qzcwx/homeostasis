function comp_base(numNode)
    format long
    fprintf('array[int] of int: base = [');
    base = 2.^(0:(numNode-1));
    for i = 1:length(base)
        if i~=length(base)
            fprintf('%.f,', base(i));
        else
            fprintf('%.f', base(i));
        end
    end
    fprintf('];\n');
end