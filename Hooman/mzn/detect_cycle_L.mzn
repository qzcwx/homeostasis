/* This is an automatically generated script for minizinc. For further information consult generate_minizinc_cycle.m function. Coded by Hooman Sedghamiz, March 2017 */
/*
This Function detects a cycle with length L given K, W and U values!
This can be applie iteratively for different lengths of L in order to detect attractors in a network iteratively
*/
%%% To use lexicographical sort library
include "globals.mzn";

%%%%%%% initialization
int: N = 6;                            
array[int] of int: ANN = [1,1,2,2];
int: M_L = 2;
set of int: M_W = {1,1,2,2};
int: Max_IE = 2;
set of int: RangeK = {0,1,2};

%%%%%%%% Here we have the K values , we look for cycles
array[int,int] of int: K_t = [|0,1,0,0
|0,1,1,1
|0,2,0,0
|0,1,2,2|];
%%%%%%%%


%%%%%%%% ADJ Matrix :Each Row [Target, Source, Threshold, Parity], if anything unknown denoted by -1
array[int,int] of int: ADJ = [|1,2,2,3,4,4
|3,1,4,2,3,4
|1,1,1,1,2,2
|0,1,0,1,1,1|];

int: NN = length(ANN);
int: S_K = pow(2,Max_IE);
array[1..N] of var bool: U;
array[1..N] of var M_W: W;
array[1..NN,1..S_K] of var RangeK: K;

% Routine Constraints
constraint forall(i in 1..NN,j in 1..S_K)(if K_t[i,j] != -1 then K[i,j]=K_t[i,j] else true endif );

constraint forall(i in 1..NN,j in 1..S_K)(K[i,j]<=ANN[i]);

constraint forall(i in 1..N)(if ADJ[4,i] != -1 then U[i] = ADJ[4,i] else true endif);

constraint forall(i in 1..N)(if ADJ[3,i] != -1 then W[i] = ADJ[3,i] else W[i] <= ANN[ADJ[2,i]] endif);



%%% Constraints about Cycle
% Here We have no prior knowledge about Cycle we check whether a cycle with Length L (e.g. 8) exists and if yes what that cycle is?
array[1..L1,1..NN] of var 0..M_L: X1;
int: L1=8; % Change L to different length to check for other attractors with different lengths
constraint forall(i in 1..L1,j in 1..NN)(X1[i,j]<=ANN[j]);

% This constraint says we look for a circular trajectory of length L
constraint forall(i in 1..NN)(X1[L1,i]=X1[1,i]);

% This constraint sorts the states lexicographically in order to avoid detection of similar cycles
%constraint forall(i in 2..L1-1)(sum(j in 1..NN)(X1[1,j]) >= sum(j in 1..NN)(X1[i,j]));

constraint forall(i in 2..L1-1)(lex_greater([X1[1,j]| j in 1..NN],[X1[i,j] | j in 1..NN]));

constraint CycleM(X1, K, W, U, M_L, NN, L1);  % This is a predicate applying Trajectory


% Constraints on K values

constraint K[1,1] <= K[1,2];
constraint K[2,1] <= K[2,2];
constraint K[2,1] <= K[2,3];
constraint K[3,1] <= K[3,2];
constraint K[4,1] <= K[4,2];
constraint K[4,1] <= K[4,3];
constraint K[4,1] <= K[4,4];
constraint K[4,2] <= K[4,4];
constraint K[4,3] <= K[4,4];


% Predicate Applying Synch or Asynch

predicate CycleM(array[int,int] of var int: X, array[int,int] of var int: K, array[int] of var int: W, array[int] of var bool: U,int: M_L, int: NN, int: L) =
forall(i in 1..L-1)(
let{
array[1..NN] of var -1..1: INDE;      % Local variable for stepwise increase or decrease
array[1..NN] of var 0..M_L: IMF;      % Image
%%% Flag function XNOR
constraint forall(m in 1..NN)(if IMF[m] < X[i,m] then
INDE[m]=-1 elseif IMF[m] > X[i,m] then
INDE[m]=1 else INDE[m]=0 endif);
% Constraints based on System of Equations

constraint IMF[1]=( K[1,1]*(1-TH(X[i,3],W[1], U[1]))+ K[1,2]*(TH(X[i,3],W[1], U[1])));

constraint IMF[2]=( K[2,1]*(1-TH(X[i,1],W[2], U[2]))*(1-TH(X[i,4],W[3], U[3]))+ K[2,2]*(TH(X[i,1],W[2], U[2]))*(1 - TH(X[i,4],W[3], U[3]))+ K[2,3]*(1 - TH(X[i,1],W[2], U[2]))*(TH(X[i,4],W[3], U[3]))+ K[2,4]*(TH(X[i,1],W[2], U[2]))*(TH(X[i,4],W[3], U[3])));

constraint IMF[3]=( K[3,1]*(1-TH(X[i,2],W[4], U[4]))+ K[3,2]*(TH(X[i,2],W[4], U[4])));

constraint IMF[4]=( K[4,1]*(1-TH(X[i,3],W[5], U[5]))*(1-TH(X[i,4],W[6], U[6]))+ K[4,2]*(TH(X[i,3],W[5], U[5]))*(1 - TH(X[i,4],W[6], U[6]))+ K[4,3]*(1 - TH(X[i,3],W[5], U[5]))*(TH(X[i,4],W[6], U[6]))+ K[4,4]*(TH(X[i,3],W[5], U[5]))*(TH(X[i,4],W[6], U[6])));

} in ( X[i+1,1] = X[i,1] + INDE[1]) /\ ( X[i+1,2] = X[i,2] + INDE[2]) /\ ( X[i+1,3] = X[i,3] + INDE[3]) /\ ( X[i+1,4] = X[i,4] + INDE[4]));

solve satisfy;
output["X1 = ",show(X1),"\n"];

function var 0..1: TH(var int: X, var int: A, var bool: B) =
let {var 0..1: F;
constraint if (X>=A /\ B) \/ (X<A /\ not(B)) then
   F = true      % Activator Above TH
else
   F = false     % Activator below TH
endif} in F;
