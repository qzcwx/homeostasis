function generate_minizinc_chen(ADJ, K, fname, stgfname)
%% This function generates a .mzn file
%% inputs: 
% ADJ:  Adjacency Matrix 4*N , where N is the number of variables in the
% network, 1st row is the Target, 2nd Row Source, 3rd Weight, 
% 4th Parity(0 Negative,1 Active,-1 notKnown) Or interaction sign  
% N :  vector of Max transcription level!
% fname = string name of the file to be created
% stgfname = string name of the file containing observed state transition
% >> Note: any unknown parameter is denoted by -1 in the matrix
%% output : 
% Generates fname.mzn in current directory of matlab.
%% Example 1:
% HPA Axis:
% ADJ =  [1 2 3 2 4 4;
%         3 1 2 4 3 4;
%         1 1 1 1 2 2;
%         0 1 1 0 1 1];
% N = [1 1 2 2];
% generate_minizinc(ADJ,N);
%% Example 2:
% HPA Axis:
% ADJ =  [1 2 3 2 4 4;
%         3 1 2 4 3 4;
%         1 1 1 1 -1 -1; % Edge weights not Known
%         0 1 -1 0 1 1]; % One parity not known
% N = [1 1 2 2];
% generate_minizinc(ADJ,N);
%% Example 3:
% HPA Axis:
% ADJ =  [1 2 3 2 4 4;
%         3 1 2 4 3 4;
%         1 1 1 1 -1 -1; % Edge weights not Known
%         0 1 -1 0 1 1]; % One parity not known
% N = [1 1 2 2];
% K = [0 -1 0 0; % Some K values known
%      0 1 1 1;
%      0 2 0 0;
%      0 -1 -1 -1;];
% generate_minizinc(ADJ,N,K);
%%

% add mzn extension if the filename doesn't end with 'mzn'
if ~strcmp(fname(end-2:end),'mzn')
   fname = [fname,'.mzn'];
end

% create a file in the folder
fid = fopen(fname,'w');
fprintf(fid,'%s\n\n',['/* This is an automatically',...
                     ' generated script for minizinc.',...
                     ' For further information consult',...
                     ' generate_minizinc.m function.',...
                     ' Coded by Hooman Sedghamiz, March 2017. ',...                     
                     ' Modified by Wenxiang Chen, June 2017 */']); 

%% Start writing the file

% number of rows == number of edges, in AJD matrix
fprintf(fid,'%s\n',['int: N = ',mat2str(size(ADJ,2)),';']);                % total number of Edges

% infer number of nodes
numNode = max(max(ADJ(1,:)), max(ADJ(2,:)));
% infer the ANN from ADJ by counting the number of out edges
N = zeros(1,numNode);
for i = ADJ(2,:)
    N(i) = N(i) + 1;
end

% infer maximal level
maxLevel = max(ADJ(3,:));

fprintf(fid,'%s','array[int] of int: ANN = [');                            % Max Transcription level
for i = 1 : length(N)
    if i ~= length(N)
    fprintf(fid,'%s',[mat2str(min(N(i),maxLevel)),',']);
    else
    fprintf(fid,'%s\n',[mat2str(min(N(i),maxLevel)),'];']);    
    end
end

fprintf(fid,'%s\n',['int: M_L = ', mat2str(min(max(N),maxLevel)) ,';']);                 % Max transc any node can possibly take

% odd number for edge threshold, singular values
fprintf(fid,'%s','set of int: M_W = {');                                   % Max possible value for edges

uniqueN = unique(N);
uniqueN = uniqueN(1:maxLevel);
for i = 1 : length(uniqueN)
    if i ~= length(uniqueN)
        fprintf(fid,'%s',[mat2str(uniqueN(i)),',']);    
    else
        fprintf(fid,'%s\n',[mat2str(uniqueN(i)),'};']);    
    end
end

% this statement was intended for E_I for each variable
% fprintf(fid,'%s','array[int] of set of int: E_I = [');    
indeg = cell(1,length(N));                                                 % Compute indegree for each variable
TL = 0;
for i = 1 : length(N)
    ind = find(ADJ(1,:) == i);
    if length(ind) > TL
       TL = length(ind);                                                   % Max indegree in net 
    end
    S = ADJ(2,ind);
    indeg{1,i}= [ind;S];
%     for j = 1: length(ind)
%        if j == 1 && (length(ind) > 1)
%           fprintf(fid,'%s',['{',mat2str(ind(j)),',']);  
%        elseif j == length(ind) && (length(ind) > 1)
%           if i < length(N)
%              fprintf(fid,'%s',[mat2str(ind(j)),'},']); 
%           else
%              fprintf(fid,'%s',[mat2str(ind(j)),'}']);  
%           end
%        elseif length(ind)==1 
%           if  i < length(N) 
%              fprintf(fid,'%s',['{',mat2str(ind(j)),'},']); 
%           else  
%              fprintf(fid,'%s',['{',mat2str(ind(j)),'}']); 
%           end
%        else
%           fprintf(fid,'%s',[mat2str(ind(j)),',']); 
%        end
%     end
end

%fprintf(fid,'%s\n','];');   

% somehow only the max indegree is needed: it is easier/probably faster
% to have a full matrix rather than a list of variant-length lists,
% see initialization of K_t
fprintf(fid,'%s\n',['int: Max_IE = ',mat2str(TL),';']);                    % Max indegree in net 

fprintf(fid,'%s','set of int: RangeK = {');                                % Set of int that K values might assume
for i = 0 : 1 : min(max(N),maxLevel)
    if i ~= min(max(N),maxLevel)
    fprintf(fid,'%s',[mat2str(i),',']);
    else
    fprintf(fid,'%s\n',[mat2str(i),'};']);    
    end
end
fprintf(fid,'\n');   


% every row in K_t represents the set of K values for a single
% node. when there are less incoming nodes than Max_IE, just fill in
% '0' for trailing cells
fprintf(fid,'%s','array[int,int] of int: K_t = [');                        % K values matrix
ff = 0;
NoC = 0; % dont post constraints on K values if we have them all

if isempty(K)
   K = zeros(length(N),2^TL);                                              % if K empty initialize 
   ff = 1;
else
   Tind = (K == -1);     % recall that -1 are unknowns
   if ~any(any(Tind))
      NoC = 1;
   end
   K(~Tind)=K(~Tind).*2; % In new logic K' = 2K;
end

for i = 1 : size(K,1)
    fprintf(fid,'%s','|');
    if ff 
      K(i,1:2^size(indeg{1,i},2)) = -1;
      %K(i,1)=0; % sets all basals to zero
    end
    for j = 1: size(K,2)
      if j ~= size(K,2)
         fprintf(fid,'%s',[mat2str(K(i,j)),',']);
      else
          if i < size(K,1)
            fprintf(fid,'%s\n',mat2str(K(i,j)));  
          else
            fprintf(fid,'%s',mat2str(K(i,j)));    
          end
      end
    end
end

fprintf(fid,'%s\n\n','|];'); 

fprintf(fid,'%s','array[int,int] of int: ADJ = [');                        % ADJ matrix

for i = 1 : size(ADJ,1)
    fprintf(fid,'%s','|');
    for j = 1: size(ADJ,2)
      if j ~= size(ADJ,2)
         fprintf(fid,'%s',[mat2str(ADJ(i,j)),',']);
      else
          if i < size(ADJ,1)
            fprintf(fid,'%s\n',mat2str(ADJ(i,j)));  
          else
            fprintf(fid,'%s',mat2str(ADJ(i,j)));    
          end
      end
    end
end
fprintf(fid,'%s\n\n','|];'); 

% %% Instantiations
% % length of ANN (number of nodes) for constructing loop
% fprintf(fid,'%s\n','int: NN = length(ANN);'); 
% % number of K values for a node
% fprintf(fid,'%s\n','int: S_K = pow(2,Max_IE);');
% % U for parity on each edge
% fprintf(fid,'%s\n','array[1..N] of var bool: U;'); 
% fprintf(fid,'%s\n','array[1..N] of var M_W: W;');
% fprintf(fid,'%s\n','array[1..NN] of var 0..M_L: X;');
% fprintf(fid,'%s\n\n','array[1..NN,1..S_K] of var RangeK: K;');

%% Here we encode biological constraints on K values
fprintf(fid,'\n\n');
fprintf(fid,'%s\n\n','% Constraints on K values');

K_ind = generate_K(N,ADJ); % generates a set of indice for K
 
if ~NoC
 % First K10 < K12 <...
 for i = 1: length(K_ind)
   for j = 1: size(K_ind{1,i},2)-1
       for k = j+1 : size(K_ind{1,i},2)
           
           % first constraint basal K values
           if j == 1 
               TF = [];
              % check for parity of all edges assoc with K
              for m = 1: size(K_ind{1,i},1)-1
                 if K_ind{1,i}(m,k) 
                    % edge sign , lazy to preallocate
                    TF = [TF ADJ(4,ind_adj(ADJ,K_ind{1,i}(m,k),i))];
                 end
              end
              if all(TF) || all(~TF)    % if edge signs are the same
                 fprintf(fid,'%s\n',...
                     ['constraint ','K[',mat2str(i),',',mat2str(j),...
                     ']',' <= ','K[',mat2str(i),',',mat2str(k),'];']);                                
              end
              
             % Now general case 
           else
              TF = [];
              L = [];
              % check for parity of all edges assoc with K
              for m = 1: size(K_ind{1,i},1)-1
                 if K_ind{1,i}(m,j) 
                    % edge sign  
                    TF = [TF ADJ(4,ind_adj(ADJ,K_ind{1,i}(m,j),i))];
                    L = [L K_ind{1,i}(m,j)];
                 end
              end
              % if the K11 has all same parity then check the rest
              if all(TF) || all(~TF) % if all activators 
                  TF1 = [];
                  L1 = [];
                 % check for parity of all edges assoc with K
                 for m = 1: size(K_ind{1,i},1)-1
                    if K_ind{1,i}(m,k) 
                        % edge sign  
                      TF1 = [TF1 ADJ(4,ind_adj(ADJ,K_ind{1,i}(m,k),i))];
                      L1 = [L1 K_ind{1,i}(m,k)];
                    end
                 end
                 % First check whether both have the same sign
                 if and(all(TF),all(TF1)) || and(all(~TF),all(~TF1))
                     % now check whether one subsumes the other
                     if length(TF) < length(TF1)
                        YN = false(1,length(TF));
                        for jj = 1: length(TF)
                            YN(jj) = any(L1 == L(jj));
                        end
                        
                        if all(YN)
                          fprintf(fid,'%s\n',...
                         ['constraint ','K[',mat2str(i),',',mat2str(j),...
                         ']',' <= ','K[',mat2str(i),',',mat2str(k),'];']);   
                        end
                     end
                 end
              end      
           end
           
       end
   end 
 end
end
fprintf(fid,'\n');

%% Encoding Base
% array[1..NN] of var 1..S_K: base = [pow(2,i-1) | i in 1..NN];
fprintf(fid, 'array[int] of int: base = [');
base = 2.^(0:(numNode-1));
for i = 1:length(base)
    if i~=length(base)
        fprintf(fid, '%d,', base(i));
    else
        fprintf(fid, '%d', base(i));
    end
end    
fprintf(fid, '];\n');

%% Encoding Cycles
stgfid = fopen(stgfname);
tline = fgets(stgfid);
count = 1;
while ischar(tline) % each line is a cycle
    tokens = strsplit(strtrim(tline),'>');
    nrow = length(tokens);
    fprintf(fid, 'int: nrow%d = %d;\n', count, nrow);
    fprintf(fid, 'array[int,int] of int: cycle%d = [|\n', count);
    for i = 1:length(tokens)
        x = char(tokens(i));
        for j = 1:length(x)
            if x(j)=='*'            
                fprintf(fid, '-1');
            else
                fprintf(fid, '%c', x(j));
            end
            if j == length(x)
                if i==length(tokens)
                    fprintf(fid, '|];\n');
                else
                    fprintf(fid, '|\n');
                end
            else
                fprintf(fid, ',');
            end
        end
    end
    fprintf(fid, 'array[1..nrow%d, 1..NN] of var 0..M_L: x%d;\n', count, count);
    fprintf(fid, 'array[1..nrow%d, 1..NN] of var 0..M_L: X%d;\n', count, count);
    fprintf(fid, 'constraint cycleconstraint(x%d, X%d, cycle%d, nrow%d);\n', count, count, count, count);
    fprintf(fid, '\n');
    
    count = count + 1;
    tline=fgets(stgfid); % get next line
end
fclose(stgfid);

system(sprintf('cat /home/chenwx/workspace/Homeostasis/ConstraintProgramming/image-cycle.mzn >> %s', fname));

fclose(fid);

end

function k = generate_K(N,Ad)
%% Generating Combinatorial K parameter Set
% I am not preallocating here cause its a combinatorial issue, If I do, I
% have to generate a matrix with the length of sum(N!/(N - r)!r!) which
% grows exponentially with the interconnectivity of the network! Now I use
% the ability of Matlab in generating Cell structure!

L = length(N);
k = cell(1,L);

 for i = 1: L  
   k{1,i}(1,1) = i;                     % Ki0 is the minimum transcription
   LI = (Ad(1,:)== i);                  % The nodes which act on Xi
   st_t = Ad(2,LI);                     % Index of Vertex acting on Xi
   LL = length(st_t);                   
   offset = 2;
   for j = 1: LL  
     ind = nchoosek(st_t,j);                           % Binomial pick up
     LJ = size(ind);
     k{1,i}(1:LJ(2),offset:offset + LJ(1) - 1) = ind'; 
     offset = offset + LJ(1);
   end 
    k{1,i}(end+1,:) = -1;
    k{1,i} = single(k{1,i});% convert to single for saving memory
 end  
end

function ind = ind_adj(ADJ,source,target)
%% returns index of an edge in ADJ
 for i = 1: size(ADJ,2)
    if ADJ(1,i) == target && ADJ(2,i) == source
       ind = i;
       break;
    end
 end

end