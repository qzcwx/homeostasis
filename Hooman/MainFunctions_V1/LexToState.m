function state = LexToState(LexInd,Levels)
%% From Lex index to State
% This function generates the permutation belonging to Lexographical Index,
% LexInd based on the bases in Levels 
% This function essential brings down the space complexity from
% O(product(Levels)) to O(1)
%% Inputs:
% Input : 
% LexInd : Lexographical Index 
% Levels : Array containing base of states (Maximum Transcription Value)
%% Outputs:
% state : Correspoding state
L = length(Levels);
state = zeros(1,L);

 for i = 1 : L - 1
     state(i) = idivide(int64(LexInd),int64(prod(Levels(i+1:L))));
     LexInd = mod(LexInd,prod(Levels(i+1:L)));
 end

state(L) = LexInd;
end