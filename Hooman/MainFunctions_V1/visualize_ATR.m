function visualize_ATR(ATR, Ad, N, k, Delay)
%% Reads in an input from DfTarjan Attractor and visualize it with DFS
% ATR : set if states or indices to visualize, can be State or
% Lexicographical Index
% Ad : Adjacency Network
% N : Levels of States
% k : Logical Parameters
% Delay : 0 is sync, 1 Async, Priority [? ? ? ?].

%% Dependency : shownetwork.m

fprintf('|#|> Converting Lexicographical ID to State...\n');

if  iscell(ATR)
  ST = [];
  CO = size(ATR,2);
  G = []; % holds vertices
  c = 1;
  %% Convert Lexicographical index to State
  for j = 1: CO     
    G = [G ATR{1,j}];  
    for i = 1:length(ATR{1,j}) 
      ST(c,:) = LexToState(ATR{1,j}(i)-1,N+1);
      c = c + 1;
    end  
  end
  
else
   error('|#|> Fatal error! First Input should be Cell!')
end

%% Reconstructed with DFS
 fprintf('|#|> Running Depth First Recosntruction...\n');
 STG = dfTraversal(N, Ad, k, ST, Delay);

%% Pack the States in a matrix
 c = 1;
 SDJ = struct;
 for i = 1:length(STG)
   if ~isempty(STG{1,i})
       for j = 1 : length(STG{1,i})
        SDJ.interaction(1,c) = i;
        SDJ.interaction(2,c) = STG{1,i}(j);
        c = c + 1;        
       end
   end
 end


 %% Get the titles for Graph
 fprintf('|#|> Getting the graph ready...\n');
 
 SDJ.titles = {};
 for i = 1:size(ST,1)
  SDJ.titles{1,i} = num2str(ST(i,:),'%d');
 end
 
 %% Convert the node number to 1:N
 for i = 1:length(G)
  LK = G(i);   
  
  ind = (SDJ.interaction(1,:) == LK);
  SDJ.interaction(3,ind) = i;
  
  ind = (SDJ.interaction(2,:) == LK);
  SDJ.interaction(4,ind) = i;
 end
 SDJ.interaction(1:2,:) =  [];

 fprintf('|#|> Initializing the Visualization...\n');
 shownetwork(SDJ);
 
 
 fprintf('|#|> Done!\n');










end