function V = INDs(M,s)
%% Fast search in a matrix for the case of a quick indexing
% M : M is the matrix, the columns are searched, just the first and last
% are searched (last is the length(s))

N = length(s);
NM = size(M,2);
V = false(1,NM);
for i = 2: NM
    if M(1,i) == s(1)
       if N > 1
           if M(N,i) == s(N)
              V(i) = true;
              break; % Index set!
           end
       else
          V(i) = true;
          break; % Index set!
       end
    end
end

end