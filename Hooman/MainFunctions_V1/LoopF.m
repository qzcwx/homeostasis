function [C,Tcycles] = LoopF(INTM)
%% detect_lp
% Detects all the feedback loops in a network (e.g. a12 a31 a23 is a loop)
% Sets of i indices (1 3 2 are equal to 2 1 3)
%% Example of input :
% C = detect_lp([1 1 1 2],[2 2 3 3 4;1 4 2 4 3;1 -1 1 -2 1]);
% C should be a matrix that contains the indice of TWO feedbacks, a34 a43, 
% and a24 a43 a32
%% Inputs
% N : vector containing the Maximum state of variables (e.g. [2 2], means
% we have two variables both having 2 states)
% INTM : 3*x matrix of Interactions where the first row is the index of 
% the node being regulated (ai_) and the second row the 
% regulating parameter (a_j), the third row is the threshold level for that
% the regulations aij is active, for inhibitors a ''-'' should be 
% attached next to the threshold, e.g. if a13 is a multiplier of a 
% inhibitor with threshold 1, then simply its value is -1

%% Outputs :
% C : Complete list of the loops seperated by a zero
%%

M = INTM(1:2,:); % Extract the indice of coeffs of the matrix 

%% Preallocation , 
k = size(M,2);                                                             % number of elements

% First Find Self-loops
A = (INTM(1,:)==INTM(2,:));
B = INTM(:,A);
C = cell(1,10000);
for i = 1 : size(B,2)
   C{1,i} = B(:,i);
end

 if ~isempty(i)
    offset = i;
 else
    offset = 0;
 end
tINTM = INTM(:,~A);
% Elementary Loops
rM = sparse(tINTM(1,:),tINTM(2,:),tINTM(3,:));
[numcycles,C1] = find_elem_circuits(rM);
for i = 1 : numcycles
  ind1 = C1{1,i}(1:end-1);
  ind2 = C1{1,i}(2:end);
  C{1,offset + i} = zeros(3,length(ind1));
  for j = 1 : length(ind1)
   th_v = and((INTM(1,:) == ind1(j)),(INTM(2,:) == ind2(j)));  
   C{1,offset + i}(1:3,j) = INTM(:,th_v); 
  end
end
clear C1;
L_c = offset + i;


%% Find Disjoined Loops
CC = C(1,1:L_c);
LLC = L_c + 1;
CO = 0;
for i = 1: L_c - 1
   temp = CC{1,i};     
    %% This Computes the unique unions
   for kp = i+1:L_c
       temp1 = CC{1,kp};
       for kkk = 1 : size(temp,2)
           if any(temp1(1,:) == temp(1,kkk))
               Doit = 0;
               break;
           else
               Doit = 1;
           end
       end
       
   if  Doit  
       final_f = [temp temp1];
       C{1,LLC} = final_f;
       LLC = LLC + 1;
     for kp1 = kp+1: L_c-1 
       temp2 = CC{1,kp1};
       for  j = 1 : size(final_f,2)         
          if any(temp2(1,:) == final_f(1,j))  
            CO = 0;  
            break;
          else
            CO = 1;
          end
       end 
       if j == size(final_f,2) && ~~CO 
          final_f = [final_f temp2]; 
          C{1,LLC} = final_f;
          LLC = LLC + 1;
       end
     end 
   end
  end


end

clear CC;

C = C(1,1:LLC-1);
Tcycles = LLC -1;

end