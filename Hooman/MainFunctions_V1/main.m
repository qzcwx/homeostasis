addpath('../ApopModel/')
load('example.mat')

% % % Detects attractors with Synchronous
% ATR = DfTarjan(example.GeneralLogic.N,example.GeneralLogic.Ad,example.GeneralLogic.k,[],0);
% visualize_ATR(ATR,example.GeneralLogic.Ad,example.GeneralLogic.N,example.GeneralLogic.k,0);
% 
% % Detects attractors with Asynchronous
% ATR = DfTarjan(example.GeneralLogic.N,example.GeneralLogic.Ad,example.GeneralLogic.k,[],1);
% visualize_ATR(ATR,example.GeneralLogic.Ad,example.GeneralLogic.N,example.GeneralLogic.k,1);

% % Detects attractors with Priority Class with Memory
% ATR = DfTarjan(example.GeneralLogic.N,example.GeneralLogic.Ad,example.GeneralLogic.k,[],[1 1 4 1]);
% visualize_ATR(ATR,example.GeneralLogic.Ad,example.GeneralLogic.N,example.GeneralLogic.k,[1 1 4 1]);