function solvestates(N, Ad, k)
% N : vector containing maximum state of each variable
% Ad : Adjaceny matrix of interactions
% Struct Containg logical parameters, if empty program generates it
% randomly Or tries to tune it automatically.


%% Fast Algorithm in Order to detect Cycles in Regulatory Nets (Johnson'S)
[C,nC] = LoopF(Ad);

if nargin <3
  k = generate_K(N,Ad,nC);  
end

 L = length(N);
 st_t = cell(L,1);
 %% Compute the incoming interactions on each Node
 INDC = zeros(nC+1,L,'single'); % holds the index of loops 
 OFFS = zeros(1,L,'single');
 CouL = cell(1,L);
 for i = 1 : L
    LI = (Ad(1,:)== i);                 % the nodes which act on XI
    st_t{i,1}(1,:) = Ad(2,LI);          % Index of the nodes acting on XI
    st_t{i,1}(2,:) = Ad(3,LI);          % Corresponding Threshold acting on XI
    INDC(1,i) = size(k{1,i},1);
    OFFS(1,i) = size(k{1,i},2);
    CouL{1,i} = false(N(i)+1,OFFS(1,i));
 end

 %test = CouL;
 %% Compute the states at edges MIN & MAX from Circuits
 BIND = N;
 counter = zeros(1,L);
 for LPS = 1 : nC
  temp = C{1,LPS};
  ind = edgeC(temp);    %computes the states at edges of circuits
 %% Now replace it in the whole state space
  SP = zeros(3,L);
  SP(1:2,temp(2,:)) = ind;
  SP(3,temp(2,:)) = temp(3,:);
 
 %% Now we need to consider all the combinations of non-loop elements
  A = (1:L);
  BB = true(1,L);
  BB(temp(2,:)) = 0;
  NNI = A(BB); %index of NON-loop elements
  
 
  if ~isempty(NNI)
    dummy_st = single(fullfact(N(BB)+1));         % all of the combinations
    dummy_st = dummy_st - 1;                      % count for zero level!
    NL_L = size(dummy_st,1);
    flag = 0;
   % sublpIND = zeros(NL_L+1,1,'single'); 
  else
    NL_L = 1; 
    flag = 1;
  end
  % preallocate
  for ffd = 1 : L
      LEND = (N(ffd)+1)*NL_L;
      CouL{1,ffd}((BIND(ffd)+1):LEND,:) = false;
      BIND(ffd) = LEND;
  end
 %% Loop and find appropriate Parameter sets
  for i = 1: NL_L
     if ~flag
       SP(1,BB) = dummy_st(i,:);        % Edge 1
       SP(2,BB) = dummy_st(i,:);        % Edge 2
     end

     for j = 1: L
         LK = size(k{1,j},2);        
         SSG = any(temp(2,:) == j);
         iter = SSG + 1;
         ind_i = st_t{j,1}(1,:);
         TH = st_t{j,1}(2,:);
         KInd = false(2,LK);
         MaxV = N(j);
         TTH = abs(SP(3,j));
         for ii = 1:iter   % for loop elements compute both edges
           st = SP(ii,ind_i);
           out = TH_F(st,TH);      % output logical of threshold function          
           st = single(ind_i(out));           % Index of Non-zero
            % See what K parameter it corresponds to  
            Lindex = length(st);
            if ~Lindex || ~all(st)
                  KInd(ii,1) = true;   % Basal Level of Expression Component
            else
                  KInd(ii,:) = INDs_mex(k{1,j},st); % precompiled C code for indexing       
            end
             %% Check out on the Logical Parameters              

              if ~SSG                        % If non-Loop Element
                   k{1,j}(end+1,KInd(ii,:)) = SP(1,j);  
                   k{1,j}(end,~KInd(ii,:)) = -1;  % for me to recognize
                   CouL{1,j}(counter(j)+SP(1,j)+1,KInd(1,:)) = true;
              else       
                   SSET = 0:MaxV; 
                   if ii == 1
                    B = SSET(SSET < TTH); % min(image) < S <= max(image)
                   else
                    B = SSET(SSET >= TTH);
                   end
                   %LAA = length(A);
                   LBB = length(B);
                   %k{1,j}(end+1:end + LAA ,KInd(1,:)) =  A;
                   %k{1,j}(end - LAA + 1 :end ,~KInd(1,:)) =  -1;
                   k{1,j}(end+1:end + LBB,KInd(ii,:)) =  B;
                   k{1,j}(end - LBB + 1 :end,~KInd(ii,:)) =  -1;
                   
                   % test
                   %CouL{1,j}(A+1,KInd(1,:)) = true;
                   CouL{1,j}(counter(j)+B+1,KInd(ii,:)) = true;
              end
            
            
            
         end
        
              INDC(LPS+1,j) = size(k{1,j},1); %keep track of each circuit parameter
%               T1 = any(CouL{1,j}); % See what constraints previous purturbation had
%               T2 = any(test{1,j}); % if T2 is all zero it means that there was contradiction 
%               CompT = and(T1,T2);
%               Final = and(test{1,j}(:,CompT),CouL{1,j}(:,CompT));
%               YN = all(any(Final));
              
     end 
     counter = counter + N +1;
  end
  % Constraint Check for local stability

  
 end 
 
 %% Constraint Check
  cc = cell(1,6);
  for Loc = 1 :L
      cc{1,Loc} = Qcount(k{1,Loc},N(Loc),INDC(1,Loc)+1);
  end
 
end


function ind = edgeC(temp) 
%% Computes the edge states for the circuit temp 
%% Temp is a matrix of 3*N,where the third row is threshold of interaction
    SS = size(temp,2);
    ind = zeros(2,SS);
    th = abs(temp(3,:));
    inhib = temp(3,:) > 0;
    for i = 1 : 2
     inhib = ~inhib;   
     ind(i,inhib) = th(inhib);
     ind(i,~inhib) = th(~inhib) -1;
    end

end


function out = TH_F(st,TH1)
%% Threshold Function : Computes from an initial state its impact
% st : Input initial state (might be a scalar or a vector)
% TH : Signed Threshold (might be a scalar or a vector)
% out : Output state ; Can be Regular if EVEN or Singular if ODD

TH = abs(TH1);
inhib = (TH1 < 0);
out = (st >= TH);
out(inhib) = 1 - out(inhib);

end

function I = computeIM(X)
%% X vector output of threshold function
%% k : logical parameters
 L = length(X);
 I = zeros(1,10000); % a big enough vector
 I(1) = (all(1 - X));
 
 C = 2;              % counter
 for i = 1:L - 1
     ind = nchoosek(1:L,i);
     Lj = size(ind,1);
     for j = 1 : Lj
      I(C) = all(X((X==X(i))))*(all(1 - X(~(X==X(i)))));
      C = C + 1;
     end
 end
 I(C) = (all(X));
 I = sum(I(1:C));
 

end

function k = generate_K(N,Ad,NC)
%% Generating Combinatorial K parameter Set
% I am not preallocating here cause its a combinatorial issue, If I do, I
% have to generate a matrix with the length of sum(N!/(N - r)!r!) which
% grows exponentially with the interconnectivity of the network! Now I use
% the ability of Matlab in generating Cell structure!

L = length(N);
k = cell(1,L);
NC = 2*NC; % double the number of cyles
 for i = 1: L  
   k{1,i}(1,1) = i;                     % Ki0 is the minimum transcription
   LI = (Ad(1,:)== i);                  % The nodes which act on Xi
   st_t = Ad(2,LI);                     % Index of Vertex acting on Xi
   LL = length(st_t);                   
   offset = 2;
   for j = 1: LL  
     ind = nchoosek(st_t,j);                           % Binomial pick up
     LJ = size(ind);
     k{1,i}(1:LJ(2),offset:offset + LJ(1) - 1) = ind'; 
     offset = offset + LJ(1);
   end 
    k{1,i}(end+1,:) = -1;
    k{1,i} = single(k{1,i});% convert to single for saving memory
 end  
end