%% Compute the whole state vector
function [IM, Delay] = IMst(st1,st_t,k,Delay)
%% Image From Current State Vector
% st : Current State Vector
% st_t : index of nodes acting on all nodes (Adjacency list) 
% k: logical parameters (K12,...)
% Delay :  String specifying the delay, 0: 'synch', 1: 'Asynch', [1 2 3
% 4]:priority

L = size(st_t,1);
IM = zeros(1,L);

 for i = 1: L
    TH = st_t{i,1};
    st = st1(TH(1,:));            % index of acting nodes
    out = TH_F(st,TH(2,:));       % states and thresholds
    ind = TH(1,out);              % index of non-zero components
    if ~isempty(ind) 
%         temp = INDs_mex(k{1,i},single(ind)); %take care Single might cause problem
        temp = INDs(k{1,i},single(ind)); %take care Single might cause problem
    else
      temp = 1;                % Means we have just Basal Level
    end
    IM(i) = k{1,i}(end,temp);     % the final image
 end
 
 %% Steady State Detected
 if IM == st1, return; end
     
 % Compute the Delay 'Synch', 'Asynch', 'Priority', 'Memory'
 [IM, Delay] = DL(IM,st1,Delay);                           
 

end


