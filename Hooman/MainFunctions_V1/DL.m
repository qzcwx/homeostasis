%% Implement the Delay Function
function [IM, Delay] = DL(IM,st1,Delay)
% IM : Computed Image
% st1 : Initial Image
% Delay :  Scalar/Vector specifying the delay, 0: 'synch', 1:'Asynch', 
% [1 2 3 4]:priority, for instance means variable one has the first
% priority. In case, that you would like to group them, just input as
% [1 1 2 2] for instance, this means the first and second belong to class
% one and the second and third belong to class 2.
% m : memory
%% Computing possible successors based on Delay Selection
%% Hooman Sedghamiz , Sep 2016
 
 if length(Delay) > 1         % Priority; We know the order (partially)
      % To be updated Here ; Use the order to update the  state vector
         ps = IM - st1;       % Direction of the movement (up low)
         index = find(~~ps);  % Non-zero values
                              %% Priority Class with Memory
      if size(Delay,1) > 1         
            tempD = Delay(:,index);
            MD = min(tempD(1,:));     % The member(s) with highest priority
            ind = (tempD(1,:) == MD); % binary index of those with min delay
            Delay(1,index(ind)) = Delay(2,index(ind)); % reset the delay of those that just updated
            iter = length(tempD(ind)); % iteration for computing updates
            iter1 = length(tempD(~ind)); %iteration for non-updatings
            ind1 = index(ind);
            ind2 = index(~ind);
            % update the states
            temp = repmat(st1,iter,1);
            for i = 1:iter
               if ps(ind1(i)) > 0
                temp(i,ind1(i)) = st1(ind1(i))+1;
                Delay(3,ind1(i)) = 1;
               else
                temp(i,ind1(i)) = st1(ind1(i))-1;
                Delay(3,ind1(i)) = 0;
               end
            end      
            %% update the memory for non-updatings
            for i = 1:iter1
               if (ps(ind2(i)) > 0 && Delay(3,ind2(i)))                       
                Delay(3,ind2(i)) = 1;
                % Moves closer to get updated
                if Delay(1,ind2(i)) > 0
                   Delay(1,ind2(i)) = Delay(1,ind2(i)) - 1; 
                end
               elseif (ps(ind2(i)) < 0 && ~Delay(3,ind2(i)))
                Delay(3,ind2(i)) = 0;
                % Moves closer to get updated
                if Delay(1,ind2(i)) > 0
                   Delay(1,ind2(i)) = Delay(1,ind2(i)) - 1; 
                end
               % If the operation is canceled or reveresed increase the delay 
               else
                if Delay(1,ind2(i)) < Delay(2,ind2(i))
                   Delay(1,ind2(i)) = Delay(1,ind2(i)) + 1; 
                end
                Delay(3,ind2(i)) = (ps(ind2(i)) > 0);
               end
            end                 
            IM = temp;
      else
                              %% Pure Priority Class without Memory        
            TempD = Delay(index);
            MD = min(TempD);
            ind = (TempD == MD);
            iter = length(TempD(ind));
            index = index(ind);
            temp = repmat(st1,iter,1);
            for i = 1:iter
               if ps(index(i)) > 0
                temp(i,index(i)) = st1(index(i))+1;
               else
                temp(i,index(i)) = st1(index(i))-1;   
               end
            end
            IM = temp;
            return;
      end
 else
     
     if ~Delay                %% Synchronous
        %return;               % One possible output IM = IM;
        ps = IM - st1;       % Direction of the movement (up low)
        index = find(~~ps);        % Non-zero values
        iter = length(index);
        temp = st1;
        for i = 1:iter
            if ps(index(i)) > 0
                temp(index(i)) = st1(index(i))+1;
            else
                temp(index(i)) = st1(index(i))-1;
            end
        end
        IM = temp;
        return;
     else                     %% Full Asynchronous
         
         ps = IM - st1;       % Direction of the movement (up low)
         index = find(~~ps);        % Non-zero values
         iter = length(index);
         temp = repmat(st1,iter,1);
         for i = 1:iter
             if ps(index(i)) > 0
              temp(i,index(i)) = st1(index(i))+1;
             else
              temp(i,index(i)) = st1(index(i))-1;   
             end
         end
         IM = temp; 
         return;
     end     
 end

end