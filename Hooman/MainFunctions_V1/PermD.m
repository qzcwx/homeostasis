function S = PermD(Levels)
%% Generates all permutations with repetitions (Recursion)
%% Input:
% Levels: is the array that holds the maximum of each level, e.g. [2
% 2],will generate 2^2 state space OR 2 nested loops where the maximum
% level of each loop is 2. [2 3], generates two nested loops through
% recursion that the first loop is (0:2)  and the second (0:3). 
%% Output: 
% S : is the state space (all Permutations) 
%% Author : Hooman Sedghamiz
% September 2016
% Caution : Watch out for overflow! The function is practical for
% product(level(1)...level(n)) <= 9000000
% In the Case of Huge space use the other two functions in order to save
% scape


WS = prod(Levels);                 % Complete State Space is the product

if WS > 9000000
   fprintf('Hey watch out the space is huge! Total possibilites %d \n',WS); 
end

k = length(Levels);
S = zeros(WS,k);
c = zeros(1,k);
b = true;
counter = 1;
while b 
  S(counter,:) = c;  
  [c,b] = increment(c, k, Levels);
  counter = counter + 1;
end
 
 
end

function [c ,b] = increment(c,k, Levels)
%% Recursion, % For Odometer %
  i = k;
  while (i>1)
    c(i) = c(i)+1;
    if c(i) == Levels(i) % i.e. MAX+1
      c(i) = 0;
      i = i-1; % incerment previous position
    else
      b = true;  
      return 
    end
  end

  % here we need to increment the first position
  c(i) = c(i) + 1;
  if c(i) == Levels(i) % we looped thru all the values
    c(i) = 0;
    b = false;
    return
  end
  b = true;
  return 
end




