%% Threshold Function : Computes from an initial state its impact
function out = TH_F(st,TH1)
% st : Input initial state (might be a scalar or a vector)
% TH : Signed Threshold (might be a scalar or a vector)
% out : Output state ;

TH = abs(TH1);
inhib = (TH1 < 0);
out = (st >= TH);
out(inhib) = 1 - out(inhib);

end