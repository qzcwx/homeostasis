function P = LexInd(state,Levels)
%% Base of the state (Max Transcription Level)
% Lexographical Indexing, from the State one can compute its Index
%% Inputs:
% Levels : base of each state in an array, e.g. [2 2 3], means the first
% two states are binary and the last is ternary
%% Output:
% P: Lexographical Index of input 'state'
 L = length(Levels);
 P = 0;
 for i = 1 : L - 1
     P = P + state(i)*prod(Levels(i+1:L));
 end
 P = P + state(L); 
end