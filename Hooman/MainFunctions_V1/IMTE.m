function IM = IMTE(st1,st_t,Delay)
 %% Image From Current State Vector
% st : Current State Vector
% st_t : index of nodes acting on all nodes (Adjacency list) 
% Delay :  String specifying the delay, 0: 'synch', 1: 'Asynch', [1 2 3
% 4]:priority

L = size(st_t,1);
IM = zeros(1,L);

 for i = 1: L
    TH = st_t{i,1};
    IN_i = TH(2,:) < 0;             % Index of Inhibitors
                                    
    
    if all(IN_i)                           % Just inhibs
       st = st1(TH(1,:));           % index of acting nodes
       IM(i) = RuleNot(RuleOR(st)); % Not(OR)
    elseif any(IN_i)                % There are Both Act and Inhib
       st = st1(TH(1,IN_i));        % Inhibitors 
       st2 = st1(TH(1,~IN_i));      % set of activator states
       IM(i) = RuleHIGH(RuleOR(st2),RuleOR(st));
    else                            % Just Activators
       st = st1(TH(1,:));           % index of acting nodes
       IM(i) = RuleOR(st);          % OR
    end
   
 end
 
 %% Steady State Detected
 if IM == st1, return; end
     
 % Compute the Delay 'Synch', 'Asynch', 'Priority', 'Memory'
 IM = DL(IM,st1,Delay);

end

function b = RuleNot(a)
%% NOT operator for finite number of states

if a > 1
    b = 0; % deactive
  elseif a < 1
    b = 2; % active    
  else   
    b = 1; % Nominal
end

end

function b = RuleOR(varargin)
% OR Operator for finite number of states

b = max(cell2mat(varargin));

end

function b = RuleHIGH(varargin)
% And Operator for finite number of states

b = (cell2mat(varargin));

if ~b(1) || b(1)==1
    if b(2) == 2 
       b = 0;
    else
       b = 1; 
    end
else
    if b(2)==2
        b = 1;        
    else
        b = 2;
    end
end

end
