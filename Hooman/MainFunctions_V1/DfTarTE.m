function [G,ATR] = DfTarTE(N,Ad,INst,Delay)
%% Use Tarjan to Consturct State Transition Graph for Ternary Logic
%% Inputs:
% N: Number of Variables, 3, 4, 5 ,etc
% Ad: Interaction Matrix,the first vector is the vertecies of
% recieving influence and the second is the index of acting node, the third
% row should be -1 if its inhibition and +1 if activation, for example, if
% node 1--|2 (node 1 inhibits node 2), [2;1;-1]; 
% INst: A set of initial states to start from,if no idea leave it empty
% Delay :  String specifying the delay, 0: 'synch', 1:'Asynch', [1 2 3
% 4]:priority 
%% Imports Stack List from Java Lib
% The same library is there for C as well
import java.util.*;

%% Start from a Initial State to reconstruct the tree

if nargin < 4
    Delay = 0; % Default is Synchronous Update
    if nargin < 3
    INst = []; % If no Initial State set, We construct the whole graph
    end
end 

L = N;
N = ones(1,L)*2;

%% Generate the List of Incoming Actions for each Node
st_t = cell(L,1);
for i = 1 : L
    LI = (Ad(1,:)== i);                 % the nodes which act on XI
    st_t{i,1}(1,:) = Ad(2,LI);          % Index of the nodes acting on XI
    st_t{i,1}(2,:) = Ad(3,LI);
end

%% Generate a 2D array to hold the State Transition Graph
% Worst Case N*(product(M)), where N: num of Node, M: Max Trans Val
B1 = prod(N+1);

if Delay
 B = L*B1;  % Asynch Freaking L times bigger!!!
else
 B = B1;    % Synchronous Graph is Small ! YaaaaY :D   
end

% Stop the Simulation in the Case of a Big graph
if B > 9000000
   fprintf('Huge Graph! %d \n Better to Use Optimization! \n We Try No Memory Allocation\n'...
       ,B);   
end


%% using globals because matlab doesn't have shared private variables

PR.tarjan_SCC = {};
PR.tarjan_stack = Stack();
ReachI = Stack(); % reachibility 
PR.index = 0;
PR.R = 1; % Counter of SCC
PR.I_R = 0; % inital R value
PR.AC = Stack(); % attractor index holder
PR.Delay = Delay;
PR.N = N;
PR.st_t = st_t;
InS = 0; % Initial size of SCC

% Setup the struct for holding flags
tarjan_v = struct();
tarjan_v.index = zeros(1,B1,'uint64');
tarjan_v.lowlink = zeros(1,B1,'uint64');
tarjan_v.onStack = false(1,B1);
tarjan_v.IND = zeros(1,B1,'uint16');

%% If the User has selected just a Specific set of initial sets
if ~isempty(INst)
 % Just Loop thro the selected Vertices
 for i = 1: size(INst,1)
     S_ind = LexInd(INst(i,:),N+1) + 1;
     if tarjan_v.index(S_ind) == 0
        [PR, tarjan_v] = strongconnect(S_ind,INst(i,:),PR,tarjan_v); 
         NS = size(tarjan_SCC,2);
        if NS > InS
         ReachI.push(NS); % Update Reachability Index
         InS = NS;
        end
     end
 end
 
else
%% loop through all nodes if no initial set specified
 for i = 1:B1
   if tarjan_v.index(i) == 0
        ST = LexToState(i-1,N+1);
        [PR, tarjan_v] = strongconnect(i,ST,PR,tarjan_v);
        NS = size(PR.tarjan_SCC,2);
        if NS > InS
         ReachI.push(NS); % Update Reachability Index
         InS = NS;
        end
   end
 end
end
%% Save the Attractors Including Cycles
 AL = PR.AC.size;                % Number of Attractors
 ATR = cell(1,AL);               % Cell Holding Attractors

 for i = 1 : AL    
    tmp = PR.AC.pop();
    ATR(1,i) = PR.tarjan_SCC(1,tmp);
 end
 
 
%% Generate the abstracted Graph Edge List
% To be Completed 
G = unique(tarjan_v.IND);

end


function [PR, tarjan_v] = strongconnect(i,ST,PR,tarjan_v)
% i : counter
% ST : The current state
% PR : struct containing the parameters of the algorithm
% tarjan_v: struct containing the checklist

% Increase the counter
PR.index = PR.index + 1;

% visit the node
tarjan_v.index(i) = PR.index;
tarjan_v.lowlink(i) = PR.index;
PR.tarjan_stack.push(i);
tarjan_v.onStack(i) = true;  % Set the neighbors on stack

out_edges = IMTE(ST,PR.st_t,PR.Delay);          % Set of neighbors 

for m = 1:size(out_edges,1)
    
    j = LexInd(out_edges(m,:),PR.N+1)+1;    % Lexicographic Indexing 
    % if not visited, visit
    if tarjan_v.index(j) == 0
        [PR, tarjan_v] = strongconnect(j,out_edges(m,:),PR,tarjan_v);
        % carry back lowlink, if lower
        tarjan_v.lowlink(i) = min(tarjan_v.lowlink([i j]));
        % record its out-edge direction
        tarjan_v.IND(i) = PR.R;
    elseif tarjan_v.onStack(j) == true
        % carry back index, if lower , root of a SCC, attractor
        tarjan_v.lowlink(i) = min([tarjan_v.lowlink(i) tarjan_v.index(j)]);
        if PR.I_R ~= PR.R, PR.AC.push(PR.R); PR.I_R = PR.R; end     % Save the index of the attractor   
        tarjan_v.IND(i) = PR.R;
    else 
        % means SCC is not an attractor and has an out-edge
        % record its out-edge direction
        tarjan_v.IND(i) = tarjan_v.IND(j);
    end
end

 if tarjan_v.lowlink(i) == tarjan_v.index(i)
    % label a new SCC
     ND = false; 
     d = 1;
     %% Start a new SCC
      while ~ND
       w = PR.tarjan_stack.pop();
       tarjan_v.onStack(w) = false;
       PR.tarjan_SCC{1,PR.R}(1,d) = w;
       d = d + 1;
       if w == i, ND = true; end % Stopping Criterion
      end
      PR.R = PR.R + 1;     % Increase the index of SCC
 end
end



