function STD = dfTravTE1(N,Ad,INst,Delay)
%% Use Depth First Search to Generate State Transition Tree with no Memory
%% Inputs:
% N: Number of Variables
% Ad: Interaction Matrix, just show, the first vector is the vertecies of
% recieving influence and the second is the index of acting node, the third
% row should be -1 if its inhibition and +1 if activation, for example, if
% node 1--|2 (node 1 inhibits node 2), [2;1;-1]; 
% INst: Optional, If user wants to see reconstructed specific states.
% Note,it should be a matrix of size M*N, where M is the number of states
% and N is the number of variables
% Delay :  String specifying the delay, 0: 'synch', 1:'Asynch', [1 2 3
% 4]:priority 
%% Output:
% ST: State Transition Graph (Adjacency List for each Node, note that the
% indices are in Lexicographical form, to convert use LexToState function
% STD : Set of Steady States! Note: This function does not return cyclic
% attractors, to detect cycles use DfTarjan.
%% Author : 
% Hooman Sedghamiz, Sep 2016
% Nova Southeastern, INIM
% hs699@nova.edu
%% Important: 
%If the size of the network is big (>16 nodes), dont save the graph and
%instead just search for the steady states, saving might make matlab go out
%of memory.
%% Imports Stack List from Java Lib
% The same library is there for C as well
import java.util.*;

%% Input Checking
if nargin < 4
    Delay = 0; % Synchronous Simulation
    if nargin < 3
    INst = []; % If no Initial State set, start from all zero
    end
end 

L = N;      % Number of Variables in Network
N = ones(1,L)*2; %Ternary
%% Generate the List of Incoming Actions for each Node (Adjacency List)
st_t = cell(L,1);
for i = 1 : L
    LI = (Ad(1,:)== i);                 % the nodes which act on XI
    st_t{i,1}(1,:) = Ad(2,LI);          % Index of the nodes acting on XI
    st_t{i,1}(2,:) = Ad(3,LI);
end

%% Generate a 2D array to hold the State Transition Graph
% Worst Case N*(product(M)), where N: num of Node, M: Max Transcription Val
B1 = prod(N+1);

if Delay
 B = L*B1;  % Asynch Freaking L times bigger!!!
else
 B = B1;    % Synchronous Graph is Small ! YaaaaY :D   
end

% Warning in the Case of a Big graph switch to non-saving 
if B > 9000000 
   fprintf('You switched to just search for the Steady States cause the Graph was Big (NR: %d)! \n',...
       B);   
end


%% Initialize the Stack and Bolean indexing
q = Stack();                % Stack for backtracking
Sq = Stack;                 % Stack holding stable states
MV = false(1,B1);           % Bolean Holder in order to know which node is visited

if isempty(INst)
% Untill all the vertices have been visited    
i = 0;
 while i < B1
  i = i + 1;           % Increase the index   
  % Pick up a seed from all possible state pool  
  if ~MV(i)
     q.push(i);             % Push the Start Node in the Stack  
        
  % Do that Recursively till we reach the leaf        
   while ~q.empty
    top = q.pop();                 % move the stack and start from top  
     
    % if the node has not been visited
    if ~MV(top)
     CS = LexToState(top - 1,N+1);  % Return the state from its Lex Index    
     MV(top) = true;                % set the node as visited 
     IM = IMTE(CS,st_t,Delay);          % Set of neighbors 
     E_L = size(IM,1);              % Edge size
     % Push All of the neighbors in Stack
     for j = 1 : E_L 
         e_ind = LexInd(IM(j,:),N+1)+1;       % Lexicographic Indexing
         if top == e_ind, Sq.push(e_ind); end % Save Steady State
         q.push(e_ind);                       % Push into Stack
     end
    end    
   end
  end
 end
%% if User just wants a specific set of initial states 
else 
    % Loop thro selected vertices
 for i = 1: size(INst,1)
     S_ind = LexInd(INst(i,:),N+1) + 1; 
  if ~MV(S_ind)
         q.push(S_ind);             % Push the Start Node in Stack 
   while ~q.empty
    top = q.pop();                 % move the stack and start from top    
    % if the node has not been visited
    if ~MV(top)
     CS = LexToState(top - 1,N+1);  % Return the state from its Lex Index   
     MV(top) = true;                % set the node as visited 
     IM = IMTE(CS,st_t,Delay);      % Set of neighbors 
     E_L = size(IM,1);              % Edge size 
     % Push All of the neighbors in Stack
      for j = 1 : E_L 
         e_ind = LexInd(IM(j,:),N+1)+1;    % Lexicographic Indexing
         if top == e_ind, Sq.push(e_ind); end % Save Steady State
         q.push(e_ind);  
      end
     end    
    end
   end
  end
end


LS = Sq.size;
STD = zeros(1,LS);
if ~Sq.empty
    % Get the Steady States in STD
    for i = 1: LS 
       STD(i) = Sq.pop();
    end
else
    % This Means there are just Cycles!
    fprintf('Sorry! No Steady State detected! \n');
end

end



