function [ST, STD] = dfTraversal(N,Ad,k,INst,Delay)
%% Use Depth First Search to Generate State Transition Tree
%% Inputs:
% N: Max Transcription Levels of Variables
% Ad: Interaction Matrix
% INst: Optional, If user wants to see reconstructed specific states.
% Note,it should be a matrix of size M*N, where M is the number of states
% and N is the number of variables

% Delay :  String specifying the delay, 0: 'synch', 1:'Asynch', [1 2 3
% 4]:priority 
%% Output:
% ST: State Transition Graph (Adjacency List for each Node, note that the
% indices are in Lexicographical form, to convert use LexToState function
% STD : Set of Steady States! Note: This function does not return cyclic
% attractors, to detect cycles use DfTarjan.
%% Author : 
% Hooman Sedghamiz, Sep 2016
% Nova Southeastern, INIM
% hs699@nova.edu

%% Imports Stack List from Java Lib
% The same library is there for C as well
fprintf('|#|> Importing Java Libraries...\n');
import java.util.*;

%% Input Checking
if nargin < 5
    Delay = 0; % Synchronous Simulation
    if nargin < 4
    INst = []; % If no Initial State set, start from all zero
    end
end 

L = length(N);      % Number of Variables in Network

%% Generate the List of Incoming Actions for each Node (Adjacency List)
st_t = cell(L,1);
for i = 1 : L
    LI = (Ad(1,:)== i);                 % the nodes which act on XI
    st_t{i,1}(1,:) = Ad(2,LI);          % Index of the nodes acting on XI
    st_t{i,1}(2,:) = Ad(3,LI);
end

%% Generate a 2D array to hold the State Transition Graph
% Worst Case N*(product(M)), where N: num of Node, M: Max Transcription Val
B1 = prod(N+1);

if Delay
 B = L*B1;  % Asynch Freaking L times bigger!!!
else
 B = B1;    % Synchronous Graph is Small ! YaaaaY :D   
end

% Warning in the Case of a Big graph
if B > 9000000
   fprintf('|#|> Fatal Error! Huge Graph! %d \n Better to Use Optimization! \n We Try No Memory Allocation\n'...
       ,B);   
end

% Adjucency List For each vertex
ST = cell(1,B1);

%% Initialize the Stack and Bolean indexing
q = Stack();                % Stack for backtracking
Sq = Stack;                 % Stack holding stable states
MV = false(1,B1);           % Bolean Holder in order to know which node is visited

if isempty(INst)
fprintf('|#|> Exploring the Whole State Transition Graph...\n');    
% Untill all the vertices have been visited    
i = 0;
 while i < B1
  i = i + 1;           % Increase the index   
  % Pick up a seed from all possible state pool  
  if ~MV(i)
     q.push(i);             % Push the Start Node in the Stack  
        
  % Do that Recursively till we reach the leaf        
   while ~q.empty
    top = q.pop();                 % move the stack and start from top  
    % if the node has not been visited
    if ~MV(top)
     CS = LexToState(top - 1,N+1);  % Return the state from its Lex Index     
     MV(top) = true;                % set the node as visited 
     IM = IMst(CS,st_t,k,Delay);    % Set of neighbors
     E_L = size(IM,1);              % Edge size
     ST{1,top} = zeros(1,E_L);      % Add the edges to the graph 
     % Push All of the neighbors in Stack
     for j = 1 : E_L 
         e_ind = LexInd(IM(j,:),N+1)+1;       % Lexicographic Indexing
         if top == e_ind, Sq.push(e_ind); end % Save Steady State
         q.push(e_ind);                       % Push into Stack
         ST{1,top}(1,j) = e_ind;
     end
    end    
   end
  end
 end
%% if User just wants a specific set of initial states 
else 
    fprintf('|#|> Exploring the selected States...\n');
    % Loop thro selected vertices
 for i = 1: size(INst,1)
     S_ind = LexInd(INst(i,:),N+1) + 1; 
  if ~MV(S_ind)
         q.push(S_ind);             % Push the Start Node in Stack 
   while ~q.empty
    top = q.pop();                 % move the stack and start from top  
  
    % if the node has not been visited
    if ~MV(top)
     CS = LexToState(top - 1,N+1);  % Return the state from its Lex Index   
     MV(top) = true;                % set the node as visited 
     IM = IMst(CS,st_t,k,Delay);    % Set of neighbors (Out Edges)
     E_L = size(IM,1);              % Edge size
     ST{1,top} = zeros(1,E_L);      % Add the edges to the graph 
     % Push All of the neighbors in Stack
      for j = 1 : E_L 
         e_ind = LexInd(IM(j,:),N+1)+1;    % Lexicographic Indexing
         if top == e_ind, Sq.push(e_ind); end % Save Steady State
         q.push(e_ind);  
         ST{1,top}(1,j) = e_ind;
      end
     end    
    end
   end
  end
end


LS = Sq.size;
STD = zeros(1,LS);
if ~Sq.empty
    % Get the Steady States in STD
    for i = 1: LS 
       STD(i) = Sq.pop();
    end
else
    % This Means there are just Cycles!
    fprintf('|#|> No Node Steady State Detected...\n');
    fprintf('|#|> Try DfTarjan it seems the network has cyclic attractors...\n');
end

fprintf('|#|> Done!\n');
end



