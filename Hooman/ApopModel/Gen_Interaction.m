function ADJ = Gen_Interaction(FileName,gr)
%% Generates Adjacency matrix from the input CSV file
%% Input:
% filename : .txt or .CSV filename of interactions from PathStudio
% gr: flag for visualization, if you want to see the graph set to 1
%% Output
% ADJ: Adjacency Mat
if nargin < 2
  gr = 1; % default shows the network  
 if nargin < 1
    FileName = uigetfile({'*.csv';'*.txt'},...
        'Select the Pathstudio file');
 else
    if ~ischar(FileName)
        error('Input should be a String!');
    else
        L = length(FileName);
        if strcmp(FileName(L-2:L),'csv') ||...
                strcmp(FileName(L-2:L),'txt')
            disp('Parsing...');
        else
            error('The file extension not supported!');
        end
    end
 end
end
%% Get a file ID
fileID = fopen(FileName,'r');
%% inline function for searching the string in a cell
cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));

%% Preallocation to 1000 Interactions! Hope its not more than that!
ADJ = struct();
ADJ.interaction = zeros(3,1000); 
ADJ.titles = cell(1,1000);
counter = 1;  % counetr of nodes
c = 0;        % counter of interactions

%% Import Important Info Line By Line
 
 while 1
  str = fgetl(fileID); % read line by line till the end
  
  if str == -1, break; end
  
  [I,E] = regexp(str,'DirectRegulation:');
  if ~isempty(I)
     c = c + 1;
     I1 = strfind(str(E+2:end),'--'); 
     S = str(E+2:E+I1-1); 
     IND = find(cellfun(cellfind(S),ADJ.titles(1:counter)));
     %% Check and see whether the Cell has been discovered
     if isempty(IND) 
         % not discovered
         ADJ.titles{counter} = S;
         IND = counter;
         counter = counter + 1;     
     end
         SI = strcmp(str(1),'p');
         if SI
            delimator = '>'; % Positive interaction
            SI = 1;
         else
            delimator = '|'; % Negative interaction
            SI = -1;
         end
         I2 = strfind(str(E+I1-1:end),delimator); 
         I2_end = strfind(str(E+I1-1+I2+1:end),'Dir');
         target = str(E+I1-1+I2+1:E+I1-1+I2+1+I2_end-3);
         IND1 = find(cellfun(cellfind(target),ADJ.titles(1:counter)));
         if isempty(IND1)
             ADJ.titles{counter} = target;
             IND1 = counter;
             counter = counter + 1;
         end
         ADJ.interaction(:,c) = [IND IND1 SI];      
  end
 end
 
fclose(fileID);
ADJ.interaction =  ADJ.interaction(:,1:c);
ADJ.titles =  ADJ.titles(:,1:counter-1);
disp('Done!');

 if gr
    shownetwork(ADJ);
 end
 
end

