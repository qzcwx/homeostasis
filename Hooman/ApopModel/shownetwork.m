function shownetwork(ADJ)
% Visualises the network in ADJ
L = size(ADJ.interaction,2);
L1 = length(ADJ.titles);
N = zeros(L1,L1);
% create a square matrix
for i = 1: L
 N(ADJ.interaction(1,i),ADJ.interaction(2,i)) = 1;
end

bg2 = biograph(N,ADJ.titles);

%% Edit the nodes style here
for i = 1: L1
   bg2.Nodes(i).Shape = 'Circle'; 
   bg2.Nodes(i).Color = [1 0.25 0.5];
   bg2.Nodes(i).Linewidth = 2.5;
   bg2.Nodes(i).LineColor = [1 1 0];%1 0.5 0
   bg2.Nodes(i).FontSize = 12;
end

%% edit Edges here
for i = 1: L
%    if ADJ.interaction(3,i) > 0
%    bg2.Edges(i).LineColor = [0 0 1]; 
%    else
%    bg2.Edges(i).LineColor = [1 0 0]; 
%    end
    bg2.Edges(i).LineColor = [0 0 1]; 
    bg2.Edges(i).LineWidth = 1.5;
end

bg2.layouttype = 'equilibrium';
view(bg2);


end