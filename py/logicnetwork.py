#!/usr/bin/python2.7
# Temporal Multi-level Discrete Logical treatment of Cell Signalling
import scipy.io
from graph_tool.all import *
import utils
import genk
import numpy as np
import random
import sys


class LogicNetwork(object):
    thres = []  # activation threshold (weight) of incoming edges to the a node
    K = []
    max_levels = []
    adj_list = []
    expected_images = dict()
    observed_images = dict()
    num_nodes = None

    def __init__(self):
        pass

    """
    Read inputs
    """

    def load_example_mat_file(self, in_mat_file):
        """ load mat file and process

        :param in_mat_file:
        :return:
        """
        mat = scipy.io.loadmat(in_mat_file)["example"]
        GeneralLogic = mat['GeneralLogic'][0][0]

        # adjacency list
        self.adj_list = GeneralLogic['Ad'][0][0]

        # maximum transcription level
        # self.max_levels = GeneralLogic['N'][0][0][0]
        self.max_levels = self.__infer_max_level(self.adj_list)
        self.num_nodes = len(self.max_levels)

        # k vector
        raw_k = GeneralLogic['k'][0][0][0]
        raw_k = [a.astype(int) for a in raw_k]

        self.__convert_threshold(self.adj_list)
        self.__convert_raw_k(raw_k)

    def load_hpag_mat_file(self, in_mat_file):
        """load hpag mat file

        :param in_mat_file:
        :return:
        """
        mat = scipy.io.loadmat(in_mat_file)
        self.adj_list = mat['HPAG'][0][0][0]

        self.max_levels = self.__infer_max_level(self.adj_list)
        self.num_nodes = len(self.max_levels)
        self.__convert_threshold(self.adj_list)

    def __load_stg(self, in_stg_file):
        """ load state transition graph (stg)
        which contains nodes with/without missing states

        :return: stg
        """
        stg = dict()
        with open(in_stg_file, 'r') as f:
            for line in f.readlines():
                tokens = line.strip().split(">")
                if tokens[0] in stg:
                    stg[tokens[0]] += tokens[1:]
                else:
                    stg[tokens[0]] = tokens[1:]
        return stg

    def load_expected_stg(self, in_stg_file):
        self.expected_images = self.__load_stg(in_stg_file)

    def load_observed_stg(self, in_stg_file):
        self.observed_images = self.__load_stg(in_stg_file)

    """
    Format conversion
    """

    def __convert_threshold(self, adj_list):
        """ Convert the .mat file to desired format

        :pre: every node has at least one incoming edge

        :return:
        """
        # go through every edges to find incoming edges
        num_row, num_col = adj_list.shape

        self.thres = [dict() for _ in range(self.num_nodes)]
        for i in xrange(num_col):
            adj = adj_list[:, i]
            self.thres[adj[0] - 1][adj[1]] = adj[2]

    def __convert_raw_k(self, raw_k):
        # todo: dict could be replaced using a binary mask to improve efficiency
        self.K = [dict() for _ in range(self.num_nodes)]
        for in_edges in raw_k:  # impact of incoming edges for all node
            for i in xrange(in_edges.shape[1]):
                in_edge = in_edges[:, i]
                if not i:  # constant term
                    key_tuple = ()
                    dict_index = in_edge[0] - 1
                else:  # non-constant term
                    # collect tuple key
                    key_list = []
                    for j in xrange(len(in_edge) - 1):
                        if not in_edge[j]:
                            break
                        key_list.append(in_edge[j])
                    key_list.sort()
                    key_tuple = tuple(key_list)
                self.K[dict_index][key_tuple] = in_edge[-1]

    def __infer_max_level(self, adj_list):
        """infer max levels from thresholds in adj_list by finding max threshold among out edges

        :param adj_list:
        :return:
        """
        adj_list_trans = adj_list.transpose()

        # infer number of nodes
        self.num_nodes = max(max(adj_list[0]), max(adj_list[1]))
        self.max_levels = [0 for i in xrange(self.num_nodes)]

        for edge_thres in adj_list_trans:
            source_node = edge_thres[1]
            threshold = edge_thres[2]

            self.max_levels[source_node - 1] = max(self.max_levels[source_node - 1], threshold)

        return self.max_levels

    """
    Logic computations
    """

    def __comp_S(self, x, theta):
        """ Threshold function

        :param x: current state as a list of ints
        :param theta:
        :return:
        """
        if x == "*":
            return None
        s = x >= abs(theta)
        if theta < 0:
            s = not s
        return s

    def comp_image(self, in_cur_state, delay='sync', k=None):
        """ compute Image of the current state

        :param in_cur_state: as string
        :param delay:
        :return:
        """
        if not k:
            k = self.K

        in_cur_state = [int(i) for i in in_cur_state]
        image_states = []
        image_states_str = []

        if delay == 'sync':
            # for each node, check activation of incoming edges
            for i in xrange(self.num_nodes):
                action_key_list = self.__comp_action_key(in_cur_state, i)
                image_states.append(k[i][action_key_list])
                # step change
                if image_states[i] < in_cur_state[i]:
                    image_states[i] = in_cur_state[i] - 1
                elif image_states[i] > in_cur_state[i]:
                    image_states[i] = in_cur_state[i] + 1
            image_states_str = ["".join([str(i) for i in image_states])]

        elif delay == 'async':
            for i in xrange(self.num_nodes):
                image_state = in_cur_state[:]
                same = False
                action_key_list = self.__comp_action_key(in_cur_state, i)
                image_state[i] = k[i][action_key_list]
                # step change
                if image_state[i] < in_cur_state[i]:
                    image_state[i] = in_cur_state[i] - 1
                elif image_state[i] > in_cur_state[i]:
                    image_state[i] = in_cur_state[i] + 1
                else:
                    same = True
                if not same:
                    image_states_str.append("".join([str(i) for i in image_state]))

        return image_states_str

    def enum_full_stg(self, k=None):
        """Enumerate Full State Transition Graph

        :return: a dict that maps every possible source node to target node(s)
        """
        # enumerate all possible states
        full_stg_edges = dict()  # state transition graph represented by edges
        cur_state = [0 for _ in xrange(self.num_nodes)]

        idx = 0
        while True:
            cur_state_str = "".join([str(i) for i in cur_state])
            full_stg_edges[cur_state_str] = self.comp_image(cur_state_str, "sync", k)

            print cur_state_str

            # move forward to the next state
            # the current bit doesn't fit, carry to the next bits
            carry = False
            while idx < self.num_nodes and cur_state[idx] == self.max_levels[idx]:
                cur_state[idx] = 0
                idx += 1
                carry = True

            if idx < self.num_nodes:
                cur_state[idx] += 1
            else:
                break

            # reset idx if carry is effective
            if carry:
                idx = 0

        return full_stg_edges

    def enum_partial_stg(self, k=None):
        """Enumerate all state transition presented in the expected images

        :param k:
        :return:
        """
        partial_stg_edges = dict()  # state transition graph represented by edges
        for source in self.expected_images.keys():
            partial_stg_edges[source] = self.comp_image(source, 'sync', k)
        return partial_stg_edges

    def __comp_action_key(self, cur_state, node):
        """given current state, compute the activated incoming edges for node

        :param cur_state:
        :param node:
        :return: None if activation status can't be determined due to missing node states
        """
        action_key_list = []
        # go through all incoming edges
        for in_edge, thres in self.thres[node].iteritems():
            S = self.__comp_S(cur_state[in_edge - 1], thres)
            if S == None:
                return None
            elif S:
                action_key_list.append(in_edge)
        return tuple(sorted(action_key_list))

    def __check_step_increment(self, images):
        """check if every directed edge satisfies the step increment requirement in synchronous update

        :return: True if satisfied; False, otherwise.
        """
        for source, targets in images.iteritems():
            for target in targets:
                for i in xrange(self.num_nodes):
                    if source[i] == '*':  # skip missing states
                        continue
                    if abs(int(source[i]) - int(target[i])) > 1:
                        return False
        return True

    def comp_num_configs(self, k_range):
        """compute number of possible configurations given in k_range

        :param k_range:
        :return:
        """
        num_config = 1L
        for one_k_range in k_range:
            for key, value in one_k_range.iteritems():
                num_config *= (value[1] - value[0] + 1)
        return num_config

    def config_k(self, observed_stg=None):
        """config k in linear time
        Based on every directed edge in the given expected state transition graph

        :param observed_stg:
        :return:
        """
        # load expected images
        if observed_stg:
            observed_images = self.load_observed_stg(observed_stg)
        else:
            observed_images = self.observed_images

        if not self.__check_step_increment(observed_images):
            return None

        # init expected K, according to incoming edges
        got_k_range = [dict() for _ in xrange(self.num_nodes)]
        for node in xrange(self.num_nodes):
            in_edges = self.thres[node].keys()
            for subset in utils.powerset(in_edges):
                got_k_range[node][subset] = [0, self.max_levels[node]]

        for source, targets in observed_images.iteritems():
            source_int = [int(i) for i in source]
            for target in targets:
                for node in xrange(self.num_nodes):
                    action_key = self.__comp_action_key(source_int, node)
                    print source, ">", target, "node", node + 1, "action key", action_key, "cur k", \
                        got_k_range[node][action_key], "target k", int(target[node])
                    if int(source[node]) < int(target[node]):
                        # stepwise increment, gives a lower bound (k could go higher)
                        got_k_range[node][action_key][0] = max(got_k_range[node][action_key][0], int(target[node]))
                    elif int(source[node]) > int(target[node]):
                        # stepwise decrement, gives an upper bound (k could go lower)
                        got_k_range[node][action_key][1] = min(got_k_range[node][action_key][1], int(target[node]))
                    else:
                        got_k_range[node][action_key] = [int(target[node]), int(target[node])]

            print 'got k after', source, ">", target
            print got_k_range

        return got_k_range

    def config_k_miss_state(self, observed_stg=None):
        """config k in linear time, in the presence of missing state
        Based on every directed edge in the given expected state transition graph

        :param observed_stg:
        :return:
        """
        # load expected images
        if observed_stg:
            observed_images = self.load_observed_stg(observed_stg)
        else:
            observed_images = self.observed_images

        if not self.__check_step_increment(observed_images):
            return None

        # init expected K, according to incoming edges
        got_k_range = [dict() for _ in xrange(self.num_nodes)]
        for node in xrange(self.num_nodes):
            in_edges = self.thres[node].keys()
            for subset in utils.powerset(in_edges):
                got_k_range[node][subset] = [0, self.max_levels[node]]

        # print "#configs", self.comp_num_configs(got_k_range)
        #
        # print 'init got k'
        # print self.print_k(got_k_range)
        # print

        for source, targets in observed_images.iteritems():
            source_int = [i for i in source]
            for i in xrange(len(source)):
                if source_int[i] != '*':
                    source_int[i] = int(source_int[i])
            for target in targets:
                for node in xrange(self.num_nodes):
                    action_key = self.__comp_action_key(source_int, node)
                    if action_key != None and target[node] != '*':
                        # action_key can be determined directly AND the current node state is known
                        # print source, ">", target, "node", node + 1
                        # print "action key", action_key
                        # print "cur k", got_k_range[node][action_key], "target k", int(target[node])
                        if int(source[node]) < int(target[node]):
                            # stepwise increment, gives a lower bound (k could go higher)
                            got_k_range[node][action_key][0] = max(got_k_range[node][action_key][0], int(target[node]))
                        elif int(source[node]) > int(target[node]):
                            # stepwise decrement, gives an upper bound (k could go lower)
                            got_k_range[node][action_key][1] = min(got_k_range[node][action_key][1], int(target[node]))
                        else:
                            got_k_range[node][action_key] = [int(target[node]), int(target[node])]

                            # print self.print_k(got_k_range)
                            # print self.comp_num_configs(got_k_range)
                            # print

        # print 'final got k'
        # print self.print_k(got_k_range)
        # print self.comp_num_configs(got_k_range)
        # print

        return got_k_range

    """
    Visualization
    """

    def viz_stg_edges(self, stg_edges, output_file):
        """visualize give state transition graph using graph-tool

        :param stg_edges:
        :param output_file:
        :return:
        """
        # set seed to get consistent layout for graph-tool
        seed_rng(1)
        np.random.seed(1)

        # print stg_edges
        stg_graph = Graph()
        label_to_vertex = dict()
        vprop = stg_graph.new_vertex_property("string")
        stg_graph.vp.label = vprop

        for source, target in stg_edges.iteritems():
            if source in label_to_vertex:
                source_v = label_to_vertex[source]
            else:
                source_v = stg_graph.add_vertex()
                stg_graph.vp.label[source_v] = source
                label_to_vertex[source] = source_v
            if type(target) == list:
                for one_target in target:
                    if one_target in label_to_vertex:
                        target_v = label_to_vertex[one_target]
                    else:
                        target_v = stg_graph.add_vertex()
                        stg_graph.vp.label[target_v] = one_target
                        label_to_vertex[one_target] = target_v
                    stg_graph.add_edge(source_v, target_v)
            else:
                if target in label_to_vertex:
                    target_v = label_to_vertex[target]
                else:
                    target_v = stg_graph.add_vertex()
                    stg_graph.vp.label[target_v] = target
                    label_to_vertex[target] = target_v

                stg_graph.add_edge(source_v, target_v)

        graph_draw(stg_graph,
                   vertex_text=stg_graph.vp.label,
                   vertex_pen_width=0.5,
                   edge_pen_width=5,
                   output=output_file)

    def viz_stg_k_range(self, k_range):
        """visualize all full state transition graphs
        corresponding to each config of given k_range

        :param k_range:
        :return:
        """
        # extract got_k from got_k_range to get every combination of k
        count = 1
        for k in genk.GenK(k_range):
            # if count == 24:
            print count, ":"
            self.viz_stg_edges(self.enum_partial_stg(k), "example-64Kconfigs/config{}-eval{}.pdf".format(count,
                                                                                                         self.eval_got_stg(
                                                                                                             k,
                                                                                                             self.expected_images)))
            count += 1

    def viz_stg_sample_k_range(self, k_range, sample_size):
        """visualize all full state transition graphs
        corresponding to each config of given k_range

        :param k_range:
        :param sample_size:
        :return:
        """

        for i in range(sample_size):
            print 'sample ', i
            sample_k = k_range
            for k in sample_k:
                for key in k.keys():
                    k[key] = random.choice(k[key])
            print sample_k
            self.viz_stg_edges(self.enum_full_stg(sample_k),
                               "hgap-50sample-configs/config{}.pdf".format(i))

    """
    Verify
    """

    def __verify_got_k(self, got_k):
        """compare the got K parameters with the expected ones

        :param got_k:
        :return: True if the same; False, otherwise.
        """
        for i in xrange(self.num_nodes):
            diff_keys = utils.diff_dict(got_k[i], self.K[i])
            if diff_keys:
                print "node", i + 1, "diff keys", diff_keys
                return False

        return True

    def eval_got_stg(self, got_k, expected_images):
        """using the got_k to generate stg, and compare with the expected one

        :param got_k:
        :param expected_images:
        :return:
        """
        eval = 0
        for source, targets in expected_images.iteritems():
            got_target = self.comp_image(source, "sync", got_k)
            if set(got_target) != set(targets):
                # print "source", source
                # print "got target", got_target
                # print "expect target", targets
                eval += 1
                # return False
        return eval

    def verify_got_stg_range(self, got_k_range, expected_stg):
        """using the got_k_range to generate stg, and compare with the expected one

        todo: need to handle missing states

        :param got_k_range: k_range instead of a single k
        :param expected_stg:
        :return:
        """
        # load expected images
        if not self.__check_step_increment(expected_stg):
            return None

        # extract got_k from got_k_range to get every combination of k
        count = 0
        for got_k in genk.GenK(got_k_range):
            count += 1
            # print 'check', got_k,
            eval = self.eval_got_stg(got_k, expected_stg)
            if eval:
                print count, 'fail, eval', eval
                # return False
            else:
                print count, 'pass'
                # self.print_k(got_k)

        return True

    """
    Print
    """

    def print_k(self, k_vec):
        for i in xrange(len(k_vec)):
            k = k_vec[i]
            for l, val in k.iteritems():
                print "K{},{} = {}".format(i + 1, l, val)

    """
    Minizinc Script Generation
    """

    @staticmethod
    def gen_attractor_constraints(self, in_stg_file):
        print "% Known state transition"
        x_set = []
        y_set = []
        with open(in_stg_file, 'r') as f:
            for line in f.readlines():
                tokens = line.strip().split(">")
                x_set.append(tokens[0])
                y_set.append(tokens[1])
        print "int: nrow =", len(x_set), ";"
        print "array[int,int] of int: x_set = [|",
        for i in xrange(len(x_set)):
            for j in xrange(len(x_set[i])):
                sys.stdout.write(str(2 * int(x_set[i][j])))
                if j == len(x_set[i]) - 1:
                    if i == len(x_set) - 1:
                        print '|];'
                    else:
                        print '|'
                else:
                    print ',',
        print "array[int,int] of int: Y_set = [|",
        for i in xrange(len(y_set)):
            for j in xrange(len(y_set[i])):
                sys.stdout.write(str(2 * int(y_set[i][j])))
                if j == len(y_set[i]) - 1:
                    if i == len(y_set) - 1:
                        print '|];'
                    else:
                        print '|'
                else:
                    print ',',

    def gen_network_info(self):
        #### part 1: bounds and parameters ####
        print "info about the network"
        print "int: N =", self.num_nodes, ";"  # number of nodes
        print "array[int] of int: ANN =", self.max_levels, ";"  # max level for each node
        print "int: M_L =", max(self.max_levels), ";"  # max level anynode can have
        # maximal weight on edge
        print "set of int: M_W = {", ", ".join([str(i) for i in list(set(self.max_levels))]), "};"
        in_degrees = [len(self.thres[i]) for i in range(self.num_nodes)]
        max_in_degree = max(in_degrees)
        print "int: Max_IE =", max_in_degree, ";"  # max indegree
        # range on k
        print "set of int: RangeK = {", ", ".join([str(i) for i in range(max(self.max_levels) + 1)]), "};"
        print

        #### part 2: matrices ####
        # initial k values
        print "array[int,int] of int: K_t = [|",
        max_num_k = 2 ** max_in_degree
        for i in range(self.num_nodes):
            l = [-1] * (2 ** in_degrees[i]) + [0] * (max_num_k - 2 ** in_degrees[i])
            for j in range(len(l)):
                sys.stdout.write(str(l[j]))
                if j == len(l) - 1:
                    if i == self.num_nodes - 1:
                        print '|];'
                    else:
                        print '|'
                else:
                    print ',',

        # adj matrix
        # separate weight value and its sign
        adj_list = self.adj_list
        sign_r = np.array([int(i > 0) for i in adj_list[2]])
        adj_list[2] = [abs(i) for i in adj_list[2]]
        adj_list = np.vstack((adj_list, sign_r))
        print "array[int,int] of int: ADJ = [|",
        for i in range(len(adj_list)):
            for j in range(len(adj_list[i])):
                sys.stdout.write(str(adj_list[i][j]))
                if j == len(adj_list[i]) - 1:
                    if i == len(adj_list) - 1:
                        print '|];'
                    else:
                        print '|'
                else:
                    print ',',

        #### part 3: constraints on k values ####
        # rule 1: base level is lower than any other states if all incoming edges signs are the same
        for i in range(self.num_nodes):
            for j in range(1, 2 ** in_degrees[i]):
                print "constraint K[{},1] <= K[{},{}]".format(i + 1, i + 1, j+1)

        # rule 2: impact of two activating/inhibiting only strength the effect

    @staticmethod
    def gen_k_constraints(self, in_stg_file):
        pass


def hpa_model():
    """Config hpa model with 4 nodes

    :return:
    """
    # input files
    example_mat_file = "../Hooman/MainFunctions_V1/example.mat"
    complete_cycle_file = "sync.cycle"
    miss_cycle_file = "sync-missing-node.cycle"

    # load data
    ln = LogicNetwork()
    ln.load_example_mat_file(example_mat_file)
    ln.load_observed_stg(miss_cycle_file)
    ln.load_expected_stg(complete_cycle_file)

    # config
    got_k_range = ln.config_k_miss_state()

    # view results
    # ln.verify_got_stg_range(got_k_range, ln.expected_images)
    ln.viz_stg_k_range(got_k_range)


def hpag_model():
    """Config hpag model with 13 nodes

    :return:
    """
    hpag_mat_file = "../Hooman/HPAG/HPAG.mat"
    observed_cycle_file = "hpag-sync.cycle"
    # hpag_cycle_file = "test.cycle"

    ln = LogicNetwork()
    ln.load_hpag_mat_file(hpag_mat_file)

    ln.load_observed_stg(observed_cycle_file)
    ln.load_expected_stg(observed_cycle_file)

    got_k_range = ln.config_k_miss_state()
    print "full k range", got_k_range
    # sample 50 from all combinations
    ln.viz_stg_sample_k_range(got_k_range, 50)


if __name__ == "__main__":
    # hpa_model()
    # hpag_model()

    example_mat_file = "../Hooman/MainFunctions_V1/example.mat"
    ln = LogicNetwork()
    ln.load_example_mat_file(example_mat_file)
    ln.gen_network_info()
    # ln.gen_attractor_constraints("sync.cycle")
